// by commy2 AGM
// modified by [WGP]Senshi
// Input params
//   weight : NUMBER - mass (as in cfg)
//   metric : STRING - desired metric (lb or kg) -- Optional, defaults to kg
//   decimals : NUMBER -desired number of decimals -- Optional, defaults to 2

#define FACTOR_POUND_TO_KILOGRAMM 1/2.2046

private ["_weight", "_metric", "_decimals"];

_weight = _this select 0;
_metric = _this select 1;
if (isNil("_metric")) then {_metric = "kg"};
// _decimals = _this select 2;
if (isNil("_decimals")) then {_decimals = 2};

if (_metric == "lb") then {
  _weight = format ["%1lb", (round (_weight * 10 ^ _decimals / 10)) / 10 ^_decimals];
} else {
    if (_metric == "g") then {
        _weight = format ["%1g", (round (_weight * FACTOR_POUND_TO_KILOGRAMM * 10 ^ _decimals * 100)) / 10 ^ _decimals];
    } else {
  		_weight = format ["%1kg", (round (_weight * FACTOR_POUND_TO_KILOGRAMM * 10 ^ _decimals / 10)) / 10 ^ _decimals];
    };
};

_weight