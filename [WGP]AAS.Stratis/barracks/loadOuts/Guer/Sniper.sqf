comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_I_GhillieSuit";
this addItemToUniform "FirstAidKit";
this addItemToUniform "HandGrenade";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_Chestrig_rgr";
for "_i" from 1 to 2 do {this addItemToUniform "9Rnd_45ACP_Mag";};
for "_i" from 1 to 4 do {this addItemToVest "5Rnd_127x108_Mag";};
for "_i" from 1 to 2 do {this addItemToVest "5Rnd_127x108_APDS_Mag";};

comment "Add weapons";
this addWeapon "srifle_GM6_F";
this addPrimaryWeaponItem "optic_LRPS";
this addWeapon "hgun_ACPC2_F";

this addItemToVest "9Rnd_45ACP_Mag";
this addItemToVest "5Rnd_127x108_Mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "NVGoggles_INDEP";