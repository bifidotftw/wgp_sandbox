private ["_base", "_list", "_radius"];
/*
_base = pV_bases select 0 select 0;
_radius = (markerSize (_base getVariable "Marker_Border") select 0) max (markerSize (_base getVariable "Marker_Border") select 1);
_list = nearestObjects [(getPos _base), ["Building", "Wall"], _radius];
{
    if ([_base, _x] call BIS_fnc_inTrigger) then {
        _x allowdamage false;
    }
} foreach _list;

_base = pV_bases select (count pv_bases -1) select 0;
_radius = (markerSize (_base getVariable "Marker_Border") select 0) max (markerSize (_base getVariable "Marker_Border") select 1);
_list = nearestObjects [(getPos _base), ["Building", "Wall"], _radius];
{
    if ([_base, _x] call BIS_fnc_inTrigger) then {
        _x allowdamage false;
    }
} foreach _list;
*/
{
    _trigger = _x select 0;
    
	_trigger setTriggerStatements [
		"vehicle player in thislist;",
		'
        if ((thisTrigger == (pV_bases select 0 select 0) and playerside == West) or (thisTrigger == (pV_bases select (count pV_bases)-1 select 0) and playerside == East)) then {
	        kitselect_action = player addaction ["<t color=""#FFFF00"">Kit Selection</t>" , "barracks\showModal.sqf","",10,false,true, "", "vehicle player == player"];
	        kitcustom_action = player addAction["<t color=""#FFFF00"">Kit Customization</t>" , "barracks\kitcustomize.sqf","",9,false,true, "", "vehicle player == player"];
        };
        ("WGP_AAS_flagprogress" call BIS_fnc_rscLayer) cutRsc ["WGP_AAS_FLAG_PROGRESS","PLAIN",1];
		script_AAS_flagprogress = [thisTrigger] spawn {
            
			uiNameSpace getVariable "AAS_Flag_ctrlProgressBar_Title" ctrlsetText ( _this select 0 getVariable "Name");
			_trigger = _this select 0;
			while {true} do {
				_points = _trigger getVariable "CapPoints";
				_capacity = _trigger getVariable "CapCapacity";
                _side = _trigger getvariable "side";
                
				_sideattack = _trigger getVariable "sideattack";
			    _color = switch (_side) do {
					case west: {team1_color};
					case east: {team2_color};
					default {
                        switch (_sideattack) do {
							case west: {team1_color};
							case east: {team2_color};
		                    default {teamneutral_color};
                    	};
                    };
				};
				uiNameSpace getVariable "AAS_Flag_ctrlProgressBar" progressSetPosition (_points / _capacity);
				uiNameSpace getVariable "AAS_Flag_ctrlProgressBar" ctrlsetTextColor _color;
				uiNameSpace getVariable "AAS_Flag_ctrlProgressBar" ctrlCommit 0;
			};
		};
		',
		'
        terminate script_AAS_flagprogress;
        ("WGP_AAS_flagprogress" call BIS_fnc_rscLayer) cutText ["", "PLAIN",1];
        player removeAction kitselect_action;
        player removeAction kitcustom_action;
        '
	];
} foreach pv_Bases;

{
    private ["_marker", "_side", "_vtype", "_trigger"];
    
	_marker = _x select 0;
    _side = _x select 1;
    if (_side == playerside) then {
	    _vtype = _x select 2;
		_trigger = createTrigger ["EmptyDetector", getMarkerPos _marker];
		_trigger setTriggerArea [(getMarkerSize _marker) select 0, (getMarkerSize _marker) select 1, markerdir _marker, if (markershape _marker == "Rectangle") then {true} else {false}];
		_trigger setTriggerActivation ["ANY", "Present", true];
        _trigger setVariable ["vtype", _vtype];
        _trigger setVariable ["side", _side];
		_trigger setTriggerStatements [
			'
			{
                if ((side group _x == (thistrigger getVariable "side")) && (_x iskindOf (thistrigger getvariable "vtype")) && ((getpos _x) select 2 < 1) && (speed _x < 1) && (local _x)) exitwith {true};
		    } foreach thislist;
		    ',
		    '
		    resupply_action = _x addAction ["<t color=""#ff9933"">Resupply vehicle</t>" , "functions\resupply\fn_resupply.sqf",_x,11,true,true, "", "abs(speed _target) < 1"];
			',
			'
		    (effectiveCommander _x) removeAction resupply_action;
		    '
		];
    };
} foreach wgp_aas_depotmarkers;


