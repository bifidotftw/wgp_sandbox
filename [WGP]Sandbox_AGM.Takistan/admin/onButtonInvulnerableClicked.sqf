closeDialog 0;

// Es gibt keine Funktion "isDamageAllowed", deshalb Workaround mit player-Variablen
if (player getVariable ["godmode", false]) then {
    player setVariable["godmode", false];
    player allowDamage false;
    player sidechat "INFO: Du bist verwundbar!";
} else {
    player setVariable["godmode", true];
	player allowDamage true;
    player sidechat "INFO: Du bist unverwundbar.";
    
};