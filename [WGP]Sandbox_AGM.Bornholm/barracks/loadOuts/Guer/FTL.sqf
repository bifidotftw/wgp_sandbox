comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "BWA3_Uniform_Fleck";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 2 do {this addItemToUniform "BWA3_DM25";};
this addItemToUniform "BWA3_30Rnd_556x45_G36";
this addItemToUniform "1Rnd_HE_Grenade_shell";
this addVest "BWA3_Vest_Leader_Fleck";
for "_i" from 1 to 4 do {this addItemToVest "BWA3_30Rnd_556x45_G36";};
for "_i" from 1 to 6 do {this addItemToVest "1Rnd_HE_Grenade_shell";};
for "_i" from 1 to 2 do {this addItemToVest "1Rnd_Smoke_Grenade_shell";};
for "_i" from 1 to 2 do {this addItemToVest "BWA3_30Rnd_556x45_G36_Tracer";};
this addHeadgear "BWA3_M92_Fleck";

comment "Add weapons";
this addWeapon "BWA3_G36_AG";
this addPrimaryWeaponItem "BWA3_acc_LLM01_flash";
this addPrimaryWeaponItem "BWA3_optic_RSAS";
this addWeapon "Binocular";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152_10";
this linkItem "ItemGPS";