comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_GhillieSuit";
this addItemToUniform "FirstAidKit";
this addItemToUniform "HandGrenade";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_Chestrig_rgr";
for "_i" from 1 to 6 do {this addItemToVest "7Rnd_408_Mag";};
for "_i" from 1 to 2 do {this addItemToVest "16Rnd_9x21_Mag";};

comment "Add weapons";
this addWeapon "srifle_LRR_camo_F";
this addPrimaryWeaponItem "optic_LRPS";
this addWeapon "hgun_P07_F";

this addItemToVest "7Rnd_408_Mag";
this addItemToVest "16Rnd_9x21_Mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "NVGoggles";