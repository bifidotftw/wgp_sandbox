comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "rhs_uniform_vdv_emr";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
for "_i" from 1 to 2 do {this addItemToUniform "rhs_mag_rdg2_white";};
this addItemToUniform "AGM_EarBuds";
this addVest "rhs_6b23_digi_crew";
this addItemToVest "rhs_mag_rgd5";
for "_i" from 1 to 4 do {this addItemToVest "rhs_30Rnd_545x39_7N10_AK";};
this addHeadgear "rhs_tsh4_ess";

comment "Add weapons";
this addWeapon "rhs_weap_aks74u";
this addWeapon "Binocular";

this addItemToVest "rhs_30Rnd_545x39_7N10_AK";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "tf_fadak";
this linkItem "ItemGPS";
this linkItem "ItemWatch";