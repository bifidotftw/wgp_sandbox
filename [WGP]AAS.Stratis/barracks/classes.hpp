#include "defines.hpp"

class WGP_BARRACK 
{
	idd = WGP_IDD_BARRACK;
	name = "WGP_BARRACK";
	movingEnable = false;
	
	controlsBackground[] = 
	{
		WGP_BARRACK_Title,
		WGP_BARRACK_Background,
        WGP_BARRACK_Weapons_List,
        WGP_BARRACK_InfoText
	};
	objects[] = {};
	controls[] =
	{
		WGP_BARRACK_SubTitle,
		WGP_BARRACK_List,
		
		WGP_BARRACK_ButtonOK,
		WGP_BARRACK_ButtonCancel
	};
    
	class WGP_BARRACK_Background: IGUIBack
	{
		idc = -1;
		x = GRID_ABS_X /2 ;
		y = GRID_ABS_Y;
		w = GRID_ABS_W;
		h = GRID_ABS_H;
	};
    
	class WGP_BARRACK_Title : RscTitle
	{
		idc = -1;
		x = GRID_ABS_X /2 ;
		y = GRID_ABS_Y - GRID_TITLE_H;
		w = GRID_ABS_W;
		h = GRID_TITLE_H;
		text = "Kits";
	};	
	class WGP_BARRACK_SubTitle : RscText
	{
		idc = WGP_IDC_BARRACK_SubTitle;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 0 * GRID_H + GRID_ABS_Y; 
		w = 0 * GRID_W;
		h = 0 * GRID_H;
		text = "";
		colorbackground[] = 
		{
			0,
			0,
			0,
			0
		};		
	};
	
	class WGP_BARRACK_List : RscListBox
	{
		idc = WGP_IDC_BARRACK_List;
		x = 0.5 * GRID_W + GRID_ABS_X /2 ; 
		y = 0 * GRID_H + GRID_ABS_Y; 
		w = 18.5 * GRID_W;
		h = 12 * GRID_H;
        onLBSelChanged = "execVM 'barracks\onList_Clicked.sqf'";
        onLBDblClick = "execVM 'barracks\onButtonOKClicked.sqf'";
	};
    
	class WGP_BARRACK_InfoText : RscStructuredText
	{
		idc = WGP_IDC_BARRACK_InfoText;
		x = 0.5 * GRID_W + GRID_ABS_X /2 ; 
		y = 12.5 * GRID_H + GRID_ABS_Y; 
		w = 18.5 * GRID_W;
		h = 6 * GRID_H;
		colorBackground[] = {0,0,0,0.3};
	};
	

	class WGP_BARRACK_ButtonOK : RscButtonMenuOK
	{
		idc = WGP_IDC_BARRACK_ButtonOK;
		x = 1 * GRID_W + GRID_ABS_X /2 ; 
		y = 19 * GRID_H + GRID_ABS_Y; 
		w = 8.9 * GRID_W;
		h = 0.9 * GRID_H;
		text = "OK";
		font = "RobotoCondensed";
		action = "execVM 'barracks\onButtonOKClicked.sqf';";
	};
	
	class WGP_BARRACK_ButtonCancel : RscButtonMenuCancel
	{
		idc = WGP_IDC_BARRACK_ButtonCancel;
		x = 10 * GRID_W + GRID_ABS_X / 2; 
		y = 19 * GRID_H + GRID_ABS_Y; 
		w = 9 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Cancel";
		font = "RobotoCondensed";
		action = "execVM 'barracks\onButtonCancelClicked.sqf';";
	};
    
    class WGP_BARRACK_Weapons_List : RscListBox
	{
		idc = WGP_IDC_BARRACK_Weapons_List;
		period = 0;
		x = 20 * GRID_W + GRID_ABS_X / 2; 
		y = 0 * GRID_H + GRID_ABS_Y; 
		w = 18.9 * GRID_W;
		h = 18.5 * GRID_H;
		colorPicture[] = {1,1,1,1};
		colorPictureSelected[] = {1,1,1,1};
		colorPictureDisabled[] = {1,1,1,1};
		colorBackground[] = {0,0,0,0.3};
		colorSelectBackground[] = {0,0,0,0};
		colorSelectBackground2[] = {0,0,0,0};
		colorText[] = {1,1,1,1};
		colorSelect[] = {1,1,1,1};
		colorSelect2[] = {1,1,1,1};
	};
};