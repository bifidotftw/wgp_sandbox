private ["_vehicle", "_aborted"];
_vehicle = _this select 3;
_aborted = false;
scopename "resupply_main";
if (local _vehicle) then {
	_type = typeOf _vehicle;
	_cfg = (configFile >> "CfgVehicles" >> _type);
	_displayname = if (isText(_cfg >> "displayName")) then { getText(_cfg >>"displayName")} else { _type };
    _magazinesTurrets = magazinesAllTurrets _vehicle; // magname, turretpath, ammocount, id, creator
    _totalnomags = count _magazinesTurrets;
    _allturrets = allturrets _vehicle;
    _allturrets pushBack [-1]; // Add driver/main vehicle as "turret"
	x_reload_time_factor = 1;
    
    _getMissingBullets = {
        private ["_name", "_count", "_maxcount"];
        _name = _this select 0; 
        _count = _this select 1;
        _maxcount = getNumber (configFile >> "CfgMagazines" >> _name >> "count");
        _maxcount - _count;
        
    };
    
    _getMagSize = {
        getNumber (configFile >> "CfgMagazines" >> _this >> "count");
    };
    
    _getMagMass = {
        _name = _this;
        getNumber(configFile >> "CfgMagazines" >> _name >> "mass");
    };
    
    _getDefMagazines = {
        private ["_name", "_turretpath", "_currentturret"];
        _name = _this select 0;
        _turretpath = _this select 1;
        _currentturret = (configFile >>"CfgVehicles" >> _name);
        {
            if (_x >= 0) then {
            _currentturret = (_currentturret >> "Turrets") select _x;
            };
        } foreach _turretpath;
        _magazines = getArray (_currentturret >> "magazines");
        _magazines;
    };
    
    {
        private ["_turretpath", "_defmags"];
        _turretpath = _x;
        _defmags = [_type, _x] call _getDefMagazines;
        _turretmags = [];
        {
            if ((_x select 1) isEqualTo _turretpath) then {
                _turretmags pushBack (_x select [0,3]);
            };
        } foreach _magazinesTurrets;
        //_turretmagscount = (_vehicle magazinesTurret _turretpath) call wgp_fnc_countUniqueArray;
        _turretmagscount = (_turretmags) call BIS_fnc_consolidateArray;
        _fullmags = [];
        _somemags = [];
        {
            private ["_name", "_turretpath", "_count"];
            _name = _x select 0 select 0;
            _turretpath = _x select 0 select 1;
            _count = _x select 0 select 2;
            _magcount = _x select 1;
            _maxcount = _name call _getMagSize;
            
            if ({_x == _name} count _defmags > 0) then {
                _missing = [_name, _count] call _getMissingBullets;
                if (_magcount > 0) then {
                    if (_missing == 0) then {
                        _fullmags pushBack _name;
                        _vehicle removeMagazineTurret [_name, _turretpath];
                    };
                };
                if (_missing > 0) then {
	                _bulletmass = (_name call _getMagMass) / _maxcount;
					for "_i" from 0 to (_missing - 1) do {
	       				if (speed _vehicle > 1) exitwith {_vehicle vehicleChat format["Reload has been ABORTED! Keep your vehicle still!"]; _aborted = true; breakto "resupply_main"};
                        //_vehicle removeMagazineTurret [_name, _turretpath];
                        sleep (x_reload_time_factor * _bulletmass);
                        _count = _count + 1;
                        _vehicle setMagazineTurretAmmo [_name, _count, _turretpath];
                        hint parseText format["<t color='#ffc600'><img image='\A3\ui_f\data\igui\cfg\actions\reammo_ca.paa'/> Reload status: %1&#37;<br/>
                        Magazines: %2 of %3 done </t>",
                        							100 * round(_count / _maxcount * 10^2) / 10^2, _foreachindex + 1, _totalnomags];
                                                   
                        //_vehicle addMagazineTurret [_name, _turretpath, _count];
                    };
                    if (_magcount > 1) then {
                        _fullmags pushBack _name;
                        _vehicle removeMagazineTurret [_name, _turretpath];
                    }
                }
                
            };
        } foreach _turretmagscount;
        {
            _vehicle addMagazineTurret [_x, _turretpath];
        } foreach _fullmags;
        if !((_vehicle magazinesTurret _turretpath) isEqualTo _defmags) then {
            _missingmags = _defmags - (_vehicle magazinesTurret _turretpath);
            {
                _vehicle addMagazineTurret [_x, _turretpath];
            } foreach _missingmags;
        }
        
    } foreach _allturrets;
    
    
    
    _vehicle vehiclechat "Rearm complete.";
    _allhitpointsdamage = getAllHitPointsDamage _vehicle;
	while { damage _vehicle > 0 or (((getAllHitPointsDamage _vehicle select 2) call BIS_fnc_greatestNum) > 0)} do {
        {
            while {_vehicle getHitPointDamage _x > 0} do {
	    if (speed _vehicle > 1) exitwith {_vehicle vehicleChat format["Repairs have been ABORTED! Keep your vehicle still!"]; _aborted = true; breakto "resupply_main"};
                hint parseText format["<t color='#ffc600'><img image='\A3\ui_f\data\gui\rsc\rscdisplayarcademap\icon_debug_ca.paa'/> Repair status: %1&#37;</t>",
                	100 * (1 - round(((getAllHitPointsDamage _vehicle select 2) call wgp_fnc_arrayaverage) * (10 ^ 2)) / (10 ^ 2))];
                _hitdmg = _vehicle getHitPointDamage _x;
                if (_hitdmg > 0.1) then {
            		_vehicle setHitPointDamage [_x, _hitdmg - 0.1];
                } else {
                    _vehicle setHitPointDamage [_x, 0];
                };
                sleep (x_reload_time_factor / 3);
            }
        } foreach (_allhitpointsdamage select 0);
	    //_vehicle setDamage ((round(damage _vehicle * (10 ^ 2)) / (10 ^ 2)  - 0.01));
        _vehicle setDamage 0;
	    //hint parseText format["<t color='#ffc600'><img image='\A3\ui_f\data\gui\rsc\rscdisplayarcademap\icon_debug_ca.paa'/> Repair status: %1&#37;</t>", 100* (1 - round(damage _vehicle * (10 ^ 2)) / (10 ^ 2))];
	    //sleep (x_reload_time_factor / 3);
	    
	};
	
	
    _vehicle vehicleChat "Repair complete.";
	while {fuel _vehicle < 0.99} do {
	    
	    if (speed _vehicle > 1) exitwith {_vehicle vehicleChat format["Refueling has been ABORTED! Keep your vehicle still!"]; _aborted = true; breakto "resupply_main"};
		_vehicle setFuel ((fuel _vehicle + 0.01));
        hint parseText format["<t color='#ffc600'><img image='\A3\ui_f\data\IGUI\Cfg\actions\refuel_ca.paa'/> Refueling: %1&#37;</t>",
                	100 * round(fuel _vehicle * (10 ^ 2)) / (10 ^ 2)];
		sleep (x_reload_time_factor);
	};
	_vehicle vehicleChat "Refuel complete.";
    hintSilent "";
	_vehicle vehicleChat format ["Maintenance for %1 complete!", _displayname];
};


if (_aborted) then {
	hintSilent "";
	_vehicle vehicleChat format ["Maintenance has NOT been completed!"];
}