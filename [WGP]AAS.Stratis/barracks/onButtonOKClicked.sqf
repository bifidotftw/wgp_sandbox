#include "defines.hpp"
WGPBarrack_ButtonOK = 1; 
WGPBarrack_Selection = lbCurSel WGP_IDC_BARRACK_List;

call compile preprocessFileLineNumbers "barracks\updateGUI.sqf";
        
_available = lbValue [WGP_IDC_BARRACK_List, WGPBarrack_Selection];

if (_available == 0) exitWith {
	systemchat ("INFO: The " + ((WGP_AAS_sideloadouts select WGPBarrack_Selection) select 0) + " kit is UNAVAILABLE to you right now!");
};
if (WGPBarrack_Selection == player getVariable "playerkitID") exitWith {
    systemchat ("INFO: You already have this kit!");
};


private["_scriptFilename"];
_scriptFilename = (WGP_AAS_sideloadouts select WGPBarrack_Selection) select 1;
systemchat ("INFO: Kit equipped: " + ((WGP_AAS_sideloadouts select WGPBarrack_Selection) select 0));
this = player;

call compile preprocessFileLineNumbers ("barracks\loadouts\" + str (playerside) + "\" + _scriptFilename + ".sqf");
this = Nil;
player setVariable ["playerKitID", WGPBarrack_Selection];
pv_playerkitid = [player, player getVariable "playerKitID"]; // player has to remember what kit he has. E.g. for kit customization.
publicvariable "pv_playerkitid"; // And everyone has to know about it!
closeDialog 0;
// call compile preprocessFileLineNumbers ("barracks\kitcustomize.sqf");