comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_PilotCoveralls";
this addItemToUniform "FirstAidKit";
this addItemToUniform "SmokeShellRed";
for "_i" from 1 to 4 do {this addItemToUniform "30Rnd_45ACP_Mag_SMG_01";};
this addBackpack "B_Parachute";
this addHeadgear "H_PilotHelmetHeli_B";

comment "Add weapons";
this addWeapon "SMG_01_F";
this addPrimaryWeaponItem "optic_Holosight_smg";

this addItemToUniform "30Rnd_45ACP_Mag_SMG_01";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "ItemGPS";
this linkItem "NVGoggles";
