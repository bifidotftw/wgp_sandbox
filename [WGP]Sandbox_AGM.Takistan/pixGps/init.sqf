if (!isServer || !isDedicated) then {
    waitUntil {!isNull player and isPlayer player };
	sleep 1;
	

    ["Group Markers", "onEachFrame", {
	// while {true} do {
        // _t1 = diag_tickTime;
    	private["_lastMarkerCount","_colors", "_sides"];
		_lastMarkerCount = 0;
	    _colors = ["ColorBLUFOR", "ColorEAST", "ColorGUER", "ColorCIV"];
	    _sides = [West, East, Resistance, Civilian];
		 private["_units", "_color", "_side"];
		_units = [playableUnits, {isPlayer _x && (side _x ==  (side player) or side _x == civilian or player call wgp_fnc_isZeus)}] call BIS_fnc_conditionalSelect; // Only register player units on own side. Civs are added, because incapacitated players are civilians (=ignored by enemy AI).
        //_units = [allunits, {(side _x ==  (side player) or side _x == civilian or player call wgp_fnc_isZeus)}] call BIS_fnc_conditionalSelect; // Only register player units on own side. Civs are added, because incapacitated players are civilians (=ignored by enemy AI).
		if (count _units != _lastMarkerCount) then {
			// Delete markers
			for "_i" from 0 to (_lastMarkerCount - 1) do {
				deleteMarkerLocal format["GpsMarker%1", _i];
			};
			
			//Recreate markers
			_lastMarkerCount = count _units;
			private ["_i", "_markerName"];
			for "_i" from 0 to (_lastMarkerCount - 1) do {
				private["_markerName"];
				_markerName = createMarkerLocal [format["GpsMarker%1", _i], [0,0,0]];
				_markerName setMarkerShapeLocal "ICON";
                _markerName setMarkerTypeLocal "mil_dot";
                _markername setMarkerDirLocal 0;
				_markerName setMarkerAlphaLocal 0.7;
				_markerName setMarkerSizeLocal [1, 1];
			};
		};
		
		// Refresh markers
		for "_i" from 0 to _lastMarkerCount-1 do {
			private["_unit"];
			_unit = _units select _i;
            if  (side _unit == (side player) or player call wgp_fnc_isZeus) then {
	            _rank = rank _unit;
				_markerName = format["GpsMarker%1", _i];
				_markerName setMarkerPosLocal (getPos _unit);
                _markername setMarkerDirLocal 0;
				_markerName setMarkerSizeLocal [1, 1];
                _markerName setMarkerTypeLocal "mil_dot";
                
				if (_unit getVariable ["AGM_isUnconscious", false]) then {
					_markerName setMarkerColorLocal "ColorBlack";
					_markerName setMarkerTextLocal "*help*";
				} else {
                    if (_sides find (side _unit) != -1) then {
                    	_color = _colors select (_sides find side _unit);
                    } else {
                        _color = "ColorBlack";
                    };
                    
					_markerName setMarkerColorLocal _color;
					
					if (_unit == leader group _unit and !isNil {group _unit getVariable "CustomGroup"}) then {
						_markerName setMarkerTypeLocal "b_unknown";
						_markerName setMarkerTextLocal groupID (group _unit);
					} else {
                        if (group _unit == group player  and !isNil {group _unit getVariable "CustomGroup"}) then {
                            _markerName setMarkerTypeLocal "mil_start";
                            _markername setMarkerDirLocal getdir _unit;
                            _markername setMarkerTextLocal "";
                            _markername setMarkerSizeLocal [0.5, 0.5];
                            _teamcolor = switch (_unit getVariable ["ST_STHud_assignedTeam","MAIN"]) do {
                                 	case "RED": {"ColorRed"};
                                    case "BLUE": {"ColorBlue"};
                                    case "GREEN": {"ColorGreen"};
                                    case "YELLOW": {"ColorYellow"};
                                    case "MAIN": {"ColorWhite"};
                                    default {"ColorWhite"};
                            };
                      		_markername setMarkerColorLocal _teamcolor;
                            
                        } else {
                            _markerName setMarkerTypeLocal "mil_dot";
                        };
                        if (player call wgp_fnc_isZeus) then {
                            _markerName setMarkerTextLocal name _unit;
                            
                        };
                    };
				};
			};
        };
        // hint str(diag_tickTime - _t1);
		// sleep 0.1;
	}] call BIS_fnc_addStackedEventHandler;
};