comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_CombatUniform_mcam_tshirt";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "HandGrenade";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_PlateCarrier2_rgr";
for "_i" from 1 to 4 do {this addItemToVest "100Rnd_65x39_caseless_mag";};
for "_i" from 1 to 3 do {this addItemToVest "100Rnd_65x39_caseless_mag_Tracer";};
this addHeadgear "H_HelmetB_grass";

comment "Add weapons";
this addWeapon "arifle_MX_SW_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Hamr";
this addPrimaryWeaponItem "bipod_01_F_snd";

this addItemToVest "100Rnd_65x39_caseless_mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "NVGoggles";