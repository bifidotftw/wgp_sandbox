

// Gate Closed
_pos = [13782.0615234,10702.0107422,-0.0351257];
_object = createVehicle ["Land_CncWall1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 179.997;
_object setPosATL _pos;

_pos = [13780.892578,10702.00585938,-0.0623627];
_object = createVehicle ["Land_CncWall1_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 179.997;
_object setPosATL _pos;

// Outer Wall
_pos = [13762.47168,10617.649414,0];
_object = createVehicle ["Land_CncWall4_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 97.0705;
_object setPosATL _pos;

_pos = [13761.708008,10612.473633,0];
_object = createVehicle ["Land_CncWall4_F", _pos, [], 0, "CAN_COLLIDE"];
_object setDir 98.0692;
_object setPosATL _pos;
