class WGP
{
	tag = "wgp";
	
	class mp
	{
		file = "mp\fnc";
		class onPlayerConnected {description = "WGP MP: Handles JIP event";}; // Function name:  wgp_fnc_onPlayerConnected
		class setRank {description = "WGP MP: Sets Rank";}; // Function name:  wgp_fnc_setRank
		class setGroupID {description = "WGP MP: Sets group ID";}; // Function name:  wgp_fnc_setGroupID
        class cleanBase {description = "WGP MP: Removes all dead units and vehicles from a marker/trigger area";};
        class preBrief {description = "WGP MP: Sets up various stuff that has to be done BEFORE the briefing."; recompile = 1; preInit = 1;};
	};
    
    class curator { // Inspired by Fett_Li - http://forums.bistudio.com/showthread.php?176691-Making-placed-units-be-editable-for-every-Zeus
        file = "mp\curator";
        class eventHandlers {description = "WGP CURATOR: Triggers on object/group placement for all Zeus"; postInit = 1;};
        class objPlaced {description = "WGP CURATOR: Makes Zeus-placed objects editable";};
        class grpPlaced {description = "WGP CURATOR: Makes Zeus-placed groups editable";};
        class isZeus {description = "WGP CURATOR: Boolean test if player is Zeus";};
    };
	
	class resupply
	{
		file = "mp\resupply";
		class resupply {description = "WGP Resupply: Resupplies vehicle (fuel, ammo, damage)";}; // Function name:  wgp_fnc_onPlayerConnected
	};
    
    class util
    {
        file = "mp\util";
        class findInString {description = "KK Util: Finds String within string";};
        class uniquearray {description = "KK Util: Returns copy of array without duplicate elements";};
        class findClass {description = "WGP Util: Find and return class of given object";};
        class str_formatWeight {description = "WGP Util: Converts abstract item mass to kg or lb format";};
    }
    
    class veh_cargoload
    {
        file = "mp\veh_cargoload";
        class getCapacity {description = "WGP Vehicle Cargo Loader: Return maximumload capacity of given object";};
        class getItemMass {descsription = "WGP Vehicle Cargo Loader: Return mass of given item";};
        class addItem {descsription = "WGP Vehicle Cargo Loader: Add/Remove item from inventory";};
        class gui_update {descsription = "WGP Vehicle Cargo Loader: Updates GUI";};
        class veh_init {descsription = "WGP Vehicle Cargo Loader: Init";};
    }

};