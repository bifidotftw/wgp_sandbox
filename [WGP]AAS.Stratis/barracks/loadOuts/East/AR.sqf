comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_O_CombatUniform_ocamo";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "HandGrenade";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_HarnessO_brn";
for "_i" from 1 to 2 do {this addItemToVest "150Rnd_762x54_Box";};
for "_i" from 1 to 2 do {this addItemToVest "150Rnd_762x54_Box_Tracer";};
this addHeadgear "H_HelmetO_ocamo";

comment "Add weapons";
this addWeapon "LMG_Zafir_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Arco";

this addItemToVest "150Rnd_762x54_Box";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "NVGoggles_OPFOR";
