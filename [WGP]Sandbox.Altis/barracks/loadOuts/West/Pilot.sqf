comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_PilotCoveralls";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
this addItemToUniform "SmokeShellBlue";
for "_i" from 1 to 4 do {this addItemToUniform "30Rnd_65x39_caseless_mag";};
this addHeadgear "H_PilotHelmetHeli_B";

comment "Add weapons";
this addWeapon "arifle_MXC_Black_F";
this addPrimaryWeaponItem "optic_Aco";
this addWeapon "Binocular";

this addItemToUniform "30Rnd_65x39_caseless_mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152";