private ["_unit", "_message"];
_unit = _this select 0;
_message = _this select 1;

if (_unit select 0 == playerside) then {
	_unit sideradio _message;
};