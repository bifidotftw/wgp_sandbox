// Hide script markers
{
	_x setmarkerAlphaLocal 0;
} foreach ["m_base", "m_base_1", "m_base_2", "m_base_3", "respawn_west","respawn_east" ,"respawn_guerrila", "respawn_civilian" ];
rep_triggers = [[repland_base, "respawn_armor"],[repheli_base,"respawn_air"],[repjet_base,"respawn_plane"]]; // Repair base triggers
call {
	{
		_trigger = _x select 0;
        _name = _x select 1;
		
		_color = "ColorBlue";
		
		_mflag = createmarkerLocal [_name, getpos _trigger];
		_mflag setmarkershapeLocal "ICON";
		_mflag setmarkertypeLocal _name;
		//_mflag setmarkertextLocal _name;
        //_mflag setmarkersizeLocal [0.1,0.1];
		_mflag setMarkerColorLocal _color;
		//_mflag setMarkerAlpha 0.5;
        
		_mborder = createmarkerLocal [_name + "_border",getpos _trigger];
		if ((triggerarea _trigger) select 3)
			then {_mborder setmarkershapeLocal "RECTANGLE" }
			else { _mborder setmarkershapeLocal "ELLIPSE"};
		_mborder setmarkersizeLocal [(triggerArea _trigger) select 0, (triggerArea _trigger) select 1];
		_mborder setMarkerDirLocal ((triggerArea _trigger) select 2) select 0;
		_mborder setMarkerColorLocal _color;
		_mborder setMarkerBrushLocal "Solid";
        _mborder setMarkerAlphaLocal 0.5;
		_trigger setVariable ["Marker1", _mflag, true];
		_trigger setVariable ["Marker2", _mborder, true];
	} forEach rep_triggers;
};


setviewdistance 2500;
setObjectViewDistance 2500;
/*setTerrainGrid 3.125;*/

waituntil {!isnil "bis_fnc_init"};
if (!isServer) then { waitUntil {!isNull player};};
enableSaving [false, false];

/*-----------------------------------------------*/
/* Parameter (0): Debug */
pixDebug = false;
if (isServer && !isDedicated) then { pixDebug = true; };
/* Parameter (0): TFR-Muted while dead */
pixParamTFARMutedOnDeath = (paramsArray select 0); //0=aus 1=an 
diag_log format["INFO: pixParamTFARMutedOnDeath: %1", pixParamTFARMutedOnDeath];
/* Parameter (1): TFR-Muted while dead */
pixParamTFARTerrainInterceptionCoefficient = (paramsArray select 1); //0,1,2,3,4,5,6,7,8,9,10
diag_log format["INFO: pixParamTFARTerrainInterceptionCoefficient: %1", pixParamTFARTerrainInterceptionCoefficient];
TF_terrain_interception_coefficient = pixParamTFARTerrainInterceptionCoefficient;
/*-----------------------------------------------*/
cutText ["Init...", "BLACK FADED",1];

// PUBLIC Variable Eventlistener

"pVAR_changeGroupID" addPublicVariableEventHandler { (_this select 1) spawn wgp_fnc_setGroupID }; //Gruppenname

"pVAR_changeRank" addPublicVariableEventHandler { (_this select 1) spawn wgp_fnc_setRank }; // Spielerrang


if (!isServer) then {
    //Server über Connect informieren
	pV_RequestClientId = player;
	publicVariableServer "PV_RequestClientId";
    // diag_log("DEBUG: JIP-ID an Server gesendet.");
};
if (hasInterface) then {
	player setVariable ["playerKitID", -1]; // player has to remember what kit he has. E.g. for kit customization.
	player addEventHandler ["Respawn", {
        if (player getVariable "playerKitID" != -1) then {
	    	_scriptFilename = loadouts select (player getVariable "playerKitID") select 1;
			systemchat ("INFO: Respawned as: " + (loadouts select (player getVariable "playerKitID") select 0));
			this = player;
			call compile preprocessFileLineNumbers ("barracks\loadouts\" + str playerside + "\" + _scriptFilename + ".sqf");
            this = Nil;
        };
    }];
};
if (isServer) then {
    //Spieler joint: Wichtige local Informationen übermitteln
	"PV_RequestClientId" addPublicVariableEventHandler {
        // diag_log("DEBUG: JIP-ID erhalten");
       _JIP_netid = owner (_this select 1);
       [_JIP_netid] spawn wgp_fnc_onPlayerConnected;  
	};
    "PV_Invisible" addPublicVariableEventHandler {
        // 0 = ClientPlayer; 1 = True/False (Invis/Vis)
        private["_player","_state"];
        _player = (_this select 1) select 0;
        _state = (_this select 1) select 1;
        _player hideObjectGlobal _state;
    };
};

/* Warten bis das Briefing beendet wurde */
sleep .1;

player setvariable ["BIS_nocoreconversations",true];

// Side Relations
Resistance setFriend [East, 0];
East setFriend [Resistance, 0];
Resistance setFriend [West, 0];
West setFriend [Resistance, 0];

/* Module initialisieren */
[] spawn compile preprocessFileLineNumbers "tfr\init.sqf"; // Task Force Radio Workarounds
[] spawn compile preprocessFileLineNumbers "admin\init.sqf"; // Admin Tools (O-Taste)
[] spawn compile preprocessFileLineNumbers "pixGps\init.sqf"; // Gruppenkartenmarker
[] spawn compile preprocessFileLineNumbers "credits\init.sqf"; // Credits
// [] spawn compile preprocessFileLineNumbers "xmedsys\init.sqf"; // XMedSys
execVM "IgiLoad\IgiLoadInit.sqf"; // IgiLoad
execVM "BDD\Greifer.sqf";
// execVM "R3F_LOG\init.sqf";
[] spawn compile preprocessFileLineNumbers "init_fireStop.sqf"; // No-Shoot-Zone in Basis
if (isServer) then { ["m_base"] spawn wgp_fnc_cleanBase; };


/* Camera deaktivieren */
cutText ["", "BLACK IN",1];