private["_building"];
_building = _this select 0;

/*-----------------------------------------------------------------	**/
/* Dialog erstellen*/
#include "defines.hpp";
createDialog "WGP_BARRACK";
// ctrlEnable [WGP_IDC_BARRACK_Weapons_List,false];

/*-----------------------------------------------------------------		*/
/* fill listbox*/
{
    private "_name";
	_name = _x select 0;
	lbAdd [WGP_IDC_BARRACK_List, _name];
    lbSetPicture [WGP_IDC_BARRACK_List, _foreachindex, [_x select 1, "img"] call wgp_fnc_getKitIcon ];
} foreach WGP_AAS_sideloadouts;
call compile preprocessFileLineNumbers "barracks\updateGUI.sqf";

/*-----------------------------------------------------------------	*/
/* Dialog anzeigen*/
WGPBarrack_ButtonOK = -1;
WGPBarrack_Selection = -1;


waituntil{sleep 0.1;!(isnull (finddisplay WGP_IDD_BARRACK))};
_dummy = "B_Soldier_F" createVehicleLocal [(getPos player select 0) + 0, (getPos player select 1) + 0, (getPos player select 2) + 0];
this = _dummy;
lbSetCurSel [WGP_IDC_BARRACK_List, 0];
_dummy setdir direction player;
player hideObject true; 
_cam = "camera" camcreate [0,0,0];
_cam cameraeffect ["internal", "back"] ;
_cam camsettarget _dummy;
_cam camsetrelpos [1.25,1.25,1.25];
//_cam camsettarget [(getpos player select 0),(getpos player select 1),(getpos player select 2)+1];
_cam camsettarget (_dummy modeltoworld [-3,0,0.75]);
_cam camcommit 0 ; 
showCinemaBorder  false;
	
if ((currentVisionMode player)==1) then { camUseNVG true;} else {camUseNVG false;};

cameraEffectEnableHUD true;

_rot=1;
waituntil {
	sleep 0.015;
	if (_rot>0) then {_dummy setdir ((direction _dummy)-0.20);};
	if (_rot<0) then {_dummy setdir ((direction _dummy)+0.20);};
	_rot=_rot+1;
	if (_rot>150) then {_rot=-150;};
	(isnull (finddisplay 850));
};

player hideObject false; 
deleteVehicle _dummy;
_cam cameraeffect ["terminate", "back"] ;
camdestroy _cam;
WGPBarrack_Selection = nil;
