comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_O_CombatUniform_ocamo";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "HandGrenade";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_TacVest_khk";
for "_i" from 1 to 10 do {this addItemToVest "10Rnd_762x54_Mag";};
this addHeadgear "H_HelmetO_ocamo";

comment "Add weapons";
this addWeapon "srifle_DMR_01_F";
this addPrimaryWeaponItem "muzzle_snds_B";
this addPrimaryWeaponItem "optic_DMS";
this addPrimaryWeaponItem "bipod_02_F_blk";

this addItemToVest "10Rnd_762x54_Mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "NVGoggles_OPFOR";
