comment "Exported from Arsenal by [WGP]Senshi";

comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "BWA3_Uniform_Crew_Fleck";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 4 do {this addItemToUniform "BWA3_40Rnd_46x30_MP7";};
this addItemToUniform "BWA3_DM32_Orange";
this addHeadgear "H_PilotHelmetHeli_B";

comment "Add weapons";
this addWeapon "BWA3_MP7";
this addHandgunItem "BWA3_acc_LLM01_flash";
this addWeapon "Binocular";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152_10";
this linkItem "ItemGPS";

this setVariable ["AGM_GForceCoef", 0.75];