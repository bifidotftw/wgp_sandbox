/*
 * Checks if TFAR is active (plugin) and if on correct server and channel
 * Disables user input and gives blackscreen if conditions not fulfilled
 */
 
while {true} do {
    private ["_plugin", "_server", "_channel"];
    _plugin = call TFAR_fnc_isTeamSpeakPluginEnabled;
    _server = TFAR_fnc_getTeamSpeakServerName;
    _channel = call TFAR_fnc_getTeamSpeakChannelName;
    
    if (_plugin && _server == "Wargamer - Project" && _channel == "TaskForceRadio") then {
        ("TFAR_Blackscreen" call BIS_fnc_rscLayer) cutFadeOut 1;
        disableUserInput false;
		disableUserInput true; // double switch as workaround for glitchy input
		disableUserInput false;
    } else {
        ("TFAR_Blackscreen" call BIS_fnc_rscLayer) titleText ["myRsc","BLACK", 999999]; //show black screen
        disableUserInput true;
    }
    
    
    
    sleep 5;
};