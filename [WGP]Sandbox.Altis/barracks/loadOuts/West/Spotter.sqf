comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_B_GhillieSuit";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
this addVest "V_Chestrig_khk";
for "_i" from 1 to 6 do {this addItemToVest "30Rnd_65x39_caseless_mag";};

comment "Add weapons";
this addWeapon "arifle_MXC_Black_F";
this addPrimaryWeaponItem "optic_Aco";
this addWeapon "Laserdesignator";

this addItemToVest "30Rnd_65x39_caseless_mag";

comment "Add items";
this linkItem "GPS";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152";