comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_I_CombatUniform";
this addItemToUniform "FirstAidKit";
this addItemToUniform "HandGrenade";
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "SmokeShellGreen";
this addVest "V_BandollierB_oli";
for "_i" from 1 to 3 do {this addItemToVest "30Rnd_556x45_Stanag";};
this addHeadgear "H_HelmetCrew_I";

comment "Add weapons";
this addWeapon "arifle_Mk20C_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_ACO_grn_smg";
this addWeapon "Binocular";

this addItemToVest "30Rnd_556x45_Stanag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "ItemGPS";
this linkItem "NVGoggles_INDEP";