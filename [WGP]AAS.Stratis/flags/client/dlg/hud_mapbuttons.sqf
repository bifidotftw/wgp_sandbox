waituntil {!isNull (finddisplay 12)};
if !hasInterface exitWith {};
disableSerialization;

private ["_idc_squads", "_idc_players", "_control"];
_idc_squads = 9991;
_idc_players = 9992;
_control = finddisplay 12 displayctrl _idc_squads;
if (isNull (finddisplay 12 displayctrl _idc_squads)) then {
	_control = findDisplay 12 ctrlCreate [ "RscButton", _idc_squads ];
} else {
    _control = finddisplay 12 displayctrl _idc_squads;
};

_control ctrlSetTextColor [0.7,0.5,0,1];
_control ctrlSetBackgroundColor [0, 1, 0, 1];
_control ctrlSetActiveColor [1, 0, 0, 1];
_control ctrlSetForegroundColor [0, 1, 0, 1];
// _pos2D = worldToScreen ( ( getPosVisual _unit ) vectorAdd [ 2 / ctrlMapScale _map , 0, 0 ] );
//_pos2D = _map ctrlMapWorldToScreen ( ( getPosVisual _unit ) vectorAdd [ 200 * (ctrlMapscale _map max 0.05), 100 * (ctrlMapscale _map max 0), 0 ] );
_control ctrlSetPosition [
	safeZoneX + safeZoneW - 0.15,
	safeZoneY + 0.15,
	0.13,
	0.05
];

_control ctrlSetText "Squads";
_control ctrlSetTooltip "Open the squad manager";
_control ctrlSetEventHandler ["ButtonClick", 'execVM "squadmgr\SHOW.sqf";'];
_control ctrlSetFade 0;
_control ctrlCommit 0;

/*

// Player overview
_control = finddisplay 12 displayctrl _idc_players;
if (isNull (finddisplay 12 displayctrl _idc_players)) then {
	_control = findDisplay 12 ctrlCreate [ "RscButton", _idc_players ];
} else {
    _control = finddisplay 12 displayctrl _idc_players;
};

_control ctrlSetTextColor [0.7,0.5,0,1];
_control ctrlSetBackgroundColor [0, 1, 0, 1];
_control ctrlSetActiveColor [1, 0, 0, 1];
_control ctrlSetForegroundColor [0, 1, 0, 1];
// _pos2D = worldToScreen ( ( getPosVisual _unit ) vectorAdd [ 2 / ctrlMapScale _map , 0, 0 ] );
//_pos2D = _map ctrlMapWorldToScreen ( ( getPosVisual _unit ) vectorAdd [ 200 * (ctrlMapscale _map max 0.05), 100 * (ctrlMapscale _map max 0), 0 ] );
_control ctrlSetPosition [
	safeZoneX + safeZoneW - 0.15,
	safeZoneY + 0.21,
	0.13,
	0.05
];

_control ctrlSetText "Players";
//_control ctrlSetTooltip "Open the player overview";
_control ctrlSetTooltip "INACTIVE - Does nothing right now";
//_control ctrlSetEventHandler ["ButtonClick", 'execVM "squadmgr\SHOW.sqf";'];
_control ctrlSetFade 0;
_control ctrlCommit 0;

*/