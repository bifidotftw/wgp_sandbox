WGP_aas_updateDepotMarkers = {
    private ["_vehicle", "_marker"];
    _vehicle = _this;
    {
        _marker = _x select 0;
		_marker setMarkerAlphaLocal 0;
    } foreach wgp_aas_depotmarkers;
    if (!(player in _vehicle) && !(local _vehicle) && !(playerside != _vehicle getVariable "side")) exitwith {
        removeMissionEventHandler ["Draw3D", wgp_aas_depotmarker_EH];
    };
	{
        _marker = _x select 0;
        _side = _x select 1;
        _vtype = _x select 2;
        if (_vehicle iskindof _vtype  && playerside == _side) then {
			_marker setMarkerAlphaLocal 1;
        };
    } foreach wgp_aas_depotmarkers;
    
    wgp_aas_depotmarker_EH = addMissionEventHandler ["Draw3D", {
        {
	        private ["_pos_marker", "_range", "_vehicletype", "_distance", "_alpha"];
			_marker = _x select 0;
			_pos_marker = getMarkerPos _marker;
			_side = _x select 1;
			_vtype = _x select 2;
			_range = 500;
			switch (_vtype) do {
			    case "LandVehicle" : {_range = 300};
			    case "Helicopter" : {_range = 500};
			    case "Plane" : {_range = 500};
			    default {_range = 500};
			};
			try {
				_distance = (getPos player) distance _pos_marker;
			} catch {
			    _distance = 1000000;
			};
			if (_distance < _range and (vehicle player iskindof _vtype) and (playerSide == _side)) then {
			    drawIcon3D [
			        switch (_vtype) do {
			            case "LandVehicle" : {"\a3\ui_f\data\map\Markers\NATO\respawn_armor_ca.paa"};
			            case "Plane" : {"\a3\ui_f\data\map\Markers\NATO\respawn_plane_ca.paa"};
			            case "Helicopter" : {"\a3\ui_f\data\map\Markers\NATO\respawn_air_ca.paa"};
			        },
			        [0.2,0.2,1,1],
			        [
			            _pos_marker select 0,
			            _pos_marker select 1,
			            6
			        ],
			        0.5 max (3 min (3 *(1 - _distance / _range))),
			        0.5 max (3 min (3 *(1 - _distance / _range))),
			        0,
			        "Maintenance",
			        1,
			        0.02 max (0.07 min (0.07 *(1 - _distance / _range))), 
			        "PuristaMedium"
				];
			};
        } foreach wgp_aas_depotmarkers;
	}];
};
	

_EH_vehicleGetIn = _this addEventHandler [ "GetIn", {
    private ["_unit", "_position", "_vehicle", "_turret"];

	_unit = _this select 2; // Should always be same as _this select 0
	_position = _this select 1; // Can be either driver, gunner, commander or cargo
	_vehicle = _this select 0;
	_turret = _this select 3;
    
    _vehicle call WGP_aas_updateDepotMarkers;
}];

_EH_vehicleGetOut = _this addEventHandler [ "GetOut", {
    private ["_unit", "_position", "_vehicle", "_turret"];

	_unit = _this select 2; // Should always be same as _this select 0
	_position = _this select 1; // Can be either driver, gunner, commander or cargo
	_vehicle = _this select 0;
	_turret = _this select 3;
    
    _unit call WGP_aas_updateDepotMarkers;
}];