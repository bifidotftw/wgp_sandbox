// Inspired by https://forums.bistudio.com/topic/153606-how-to-turn-off-thermals/
// edited by [WGP]Senshi

if (!isNil "scr_disablethermals") then {
	terminate scr_disablethermals;
};

scr_disablethermals = [] spawn {
	    private ["_layer", "_launchers"];
	
	
	_layer = ("disablethermaloverlay" call BIS_fnc_rscLayer);
	_launchers =["launch_I_Titan_short_F","launch_O_Titan_short_F","launch_B_Titan_short_F","launch_Titan_short_F","launch_Titan_F","launch_O_Titan_F","launch_I_Titan_F","launch_B_Titan_F"];  // launcher array
	
	
	while {true} do 
	{
	    if (vehicle player != player) then {
	        vehicle player disableTIEquipment true;
	    };
	if (currentVisionMode player == 2) then																		// check for TI Mode
	{
		/*
	    _PP_colorC = ppEffectCreate ["ColorCorrections",1500];
		_PP_colorC ppEffectEnable true;
		_PP_colorC ppEffectAdjust [-5,0,0,[0,0,0,0],[1,1,1,1],[0,0,0,0],[0,0,0,0,0,0,4]];
		_PP_colorC ppEffectCommit 0;
		_PP_film = ppEffectCreate ["FilmGrain",2000];
		_PP_film ppEffectEnable true;
		_PP_film ppEffectAdjust [1,0,8,2,2,true];
		_PP_film ppEffectCommit 0;
		// Date YYYY-MM-DD-HH-MM: [2035,6,24,8,0]. Overcast: 0.3. Fog: 0.0819915. Fog params: [0.0800022,0.013,0] 
		// GF PostProcess Editor parameters: Copy the following line to clipboard and click Import in the editor.
		//[[false,100,[0.05,0.05,0.3,0.3]],[false,200,[0.1,0.1,true]],[false,300,[2,0.4,0.4,2,1,-2,-1,0.1,0.01,0.1,0.01,0.02,0.05,1,1]],[true,1500,[1,1,0,[0,0,0,0],[1,1,1,1],[0,0,0,0],[0,0,0,0,0,0,4]]],[false,500,[0]],[true,2000,[1,0,8,2,2,true]],[false,2500,[1,1,1]]]
		*/
		setViewDistance 1;				
		playSound "FD_CP_Not_Clear_F";
	    _layer cutText ["Thermal Imaging OFFLINE! Only Day/NV are available!","BLACK",0.1];		// Blackout screen with Text
	    waituntil {currentVisionMode player != 2}; // Once viewmode left, return everything to default
		setViewDistance -1;
	    //ppEffectDestroy _PP_colorC;
	    //ppEffectDestroy _PP_film;
	    _layer cutText ["", "PLAIN"];
	};	
		
		sleep 1;
	};
};