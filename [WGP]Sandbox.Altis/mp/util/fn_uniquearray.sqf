/*
 * Author:
 * Killzone_Kid
 * 
 * Description:
 *  Returns a copy of the input array where each element is unique
 * 
 * Parameters:
 * _this : Array
 * 
 * Returns:
 * Array : Copy of _this, with all duplicate entries removed
 */
 
private "_arr";
_arr = [];
while {!(_this isEqualTo [])} do {
	_arr pushBack (_this select 0);
	_this = _this - _arr;
};
_arr