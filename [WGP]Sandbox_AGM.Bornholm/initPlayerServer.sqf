/* This script runs on server whenever a player joins.
 * Meaning it runs on game start as well as for every JIP.
 * https://community.bistudio.com/wiki/Event_Scripts
 */

{    _x addCuratorEditableObjects [[_this select 0],true];} forEach allCurators; // Makes JIP players editable for Zeus -- Inspired by Fett_Li http://forums.bistudio.com/showthread.php?176691-Making-placed-units-be-editable-for-every-Zeus
 