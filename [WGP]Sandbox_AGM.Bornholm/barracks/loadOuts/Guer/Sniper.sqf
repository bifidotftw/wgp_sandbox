comment "Exported from Arsenal by [WGP]Senshi";

comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "BWA3_Uniform_Fleck";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
this addItemToUniform "BWA3_optic_NSV600";
for "_i" from 1 to 2 do {this addItemToUniform "BWA3_DM25";};
this addVest "BWA3_Vest_Rifleman1_Fleck";
for "_i" from 1 to 5 do {this addItemToVest "BWA3_10Rnd_762x51_G28_Tracer";};
for "_i" from 1 to 8 do {this addItemToVest "BWA3_10Rnd_762x51_G28";};
for "_i" from 1 to 3 do {this addItemToVest "BWA3_10Rnd_762x51_G28_AP";};
this addHeadgear "BWA3_OpsCore_Fleck";

comment "Add weapons";
this addWeapon "BWA3_G28_Standard";
this addPrimaryWeaponItem "optic_SOS";
this addWeapon "Binocular";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "tf_anprc152_10";