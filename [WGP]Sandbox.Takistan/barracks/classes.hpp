#include "defines.hpp"

class PIXLOGISTIC_DIALOG_BARRACK 
{
	idd = PIXLOGISTIC_IDD_DIALOG_BARRACK;
	name = "PIXLOGISTIC_DIALOG_BARRACK";
	movingEnable = false;
	
	controlsBackground[] = 
	{
		PIXLOGISTIC_DIALOG_BARRACK_Title,
		PIXLOGISTIC_DIALOG_BARRACK_Background,
        PIXLOGISTIC_DIALOG_BARRACK_Weapons_List
	};
	objects[] = {};
	controls[] =
	{
		PIXLOGISTIC_DIALOG_BARRACK_SubTitle,
		PIXLOGISTIC_DIALOG_BARRACK_List,
		
		PIXLOGISTIC_DIALOG_BARRACK_ButtonOK,
		PIXLOGISTIC_DIALOG_BARRACK_ButtonCancel
	};
    
	class PIXLOGISTIC_DIALOG_BARRACK_Background: IGUIBack
	{
		idc = -1;
		x = GRID_ABS_X /2 ;
		y = GRID_ABS_Y;
		w = GRID_ABS_W;
		h = GRID_ABS_H;
	};
    
	class PIXLOGISTIC_DIALOG_BARRACK_Title : RscTitle
	{
		idc = -1;
		x = GRID_ABS_X /2 ;
		y = GRID_ABS_Y - GRID_TITLE_H;
		w = GRID_ABS_W;
		h = GRID_TITLE_H;
		text = "Kits";
	};	
	class PIXLOGISTIC_DIALOG_BARRACK_SubTitle : RscText
	{
		idc = PIXLOGISTIC_IDC_DIALOG_BARRACK_SubTitle;
		x = 1 * GRID_W + GRID_ABS_X; 
		y = 0 * GRID_H + GRID_ABS_Y; 
		w = 0 * GRID_W;
		h = 0 * GRID_H;
		text = "";
		colorbackground[] = 
		{
			0,
			0,
			0,
			1
		};		
	};
	
	class PIXLOGISTIC_DIALOG_BARRACK_List : RscListBox
	{
		idc = PIXLOGISTIC_IDC_DIALOG_BARRACK_List;
		x = 1 * GRID_W + GRID_ABS_X /2 ; 
		y = 0 * GRID_H + GRID_ABS_Y; 
		w = 18 * GRID_W;
		h = 18.5 * GRID_H;
        onLBSelChanged = "execVM 'barracks\onList_Clicked.sqf'";
	};
	

	class PIXLOGISTIC_DIALOG_BARRACK_ButtonOK : RscButtonMenuOK
	{
		idc = PIXLOGISTIC_IDC_DIALOG_BARRACK_ButtonOK;
		x = 1 * GRID_W + GRID_ABS_X /2 ; 
		y = 19 * GRID_H + GRID_ABS_Y; 
		w = 8.9 * GRID_W;
		h = 0.9 * GRID_H;
		text = "OK";
		action = "execVM 'barracks\onButtonOKClicked.sqf';";
	};
	
	class PIXLOGISTIC_DIALOG_BARRACK_ButtonCancel : RscButtonMenuCancel
	{
		idc = PIXLOGISTIC_IDC_DIALOG_BARRACK_ButtonCancel;
		x = 10 * GRID_W + GRID_ABS_X / 2; 
		y = 19 * GRID_H + GRID_ABS_Y; 
		w = 9 * GRID_W;
		h = 0.9 * GRID_H;
		text = "Abbrechen";
		action = "execVM 'barracks\onButtonCancelClicked.sqf';";
	};
    
    class PIXLOGISTIC_DIALOG_BARRACK_Weapons_List : RscListBox
	{
		idc = PIXLOGISTIC_IDC_DIALOG_BARRACK_Weapons_List;
		x = 20 * GRID_W + GRID_ABS_X / 2; 
		y = 0 * GRID_H + GRID_ABS_Y; 
		w = 18.9 * GRID_W;
		h = 18.5 * GRID_H;
        /*colorBackground[] = {0.2,0.2,0.2,0.5}; // Fill color
		colorSelectBackground[] = {0.2,0.2,0.2,0.5}; // Selected item fill color
		colorSelectBackground2[] = {0.2,0.2,0.2,0.5}; // Selected item fill color (oscillates between this and colorSelectBackground)
		colorText[] = {1,1,1,1}; // Text and frame color
		colorDisabled[] = {1,1,1,1}; // Disabled text color
		colorSelect[] = {1,1,1,1}; // Text selection color
		colorSelect2[] = {1,1,1,1}; // Text selection color (oscillates between this and colorSelect)*/
	};
};