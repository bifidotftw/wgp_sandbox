comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_O_CombatUniform_ocamo";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "HandGrenade";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_HarnessOGL_brn";
for "_i" from 1 to 6 do {this addItemToVest "30Rnd_65x39_caseless_green";};
for "_i" from 1 to 10 do {this addItemToVest "1Rnd_HE_Grenade_shell";};
for "_i" from 1 to 5 do {this addItemToVest "1Rnd_Smoke_Grenade_shell";};
this addHeadgear "H_HelmetLeaderO_ocamo";

comment "Add weapons";
this addWeapon "arifle_Katiba_GL_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Arco";

this addItemToVest "30Rnd_65x39_caseless_green";
this addItemToVest "1Rnd_HE_Grenade_shell";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "NVGoggles_OPFOR";
