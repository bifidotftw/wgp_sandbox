#include "defines.hpp"

_int_change = _this;
_display = finddisplay WGP_VEH_CargoLoad_IDD;
_listbox = _display displayctrl WGP_VEH_CargoLoad_LISTNBOX_IDC;
_select_name = _listbox lnbText [lnbCurSelRow WGP_VEH_CargoLoad_LISTNBOX_IDC,1];
_select_item = wgp_vehcargoload_inventory_unique select (lnbCurSelRow WGP_VEH_CargoLoad_LISTNBOX_IDC);
_item_type = _select_item call BIS_fnc_itemType;
// systemchat str _item_type;

// systemchat _select_item;
if (_int_change < 0 ) then {
    if ((wgp_vehcargoload_inventory find _select_item) == -1) exitWith {
        
    };
	// hint (" Removed 1 " + _select_name);
    // systemchat str (wgp_vehcargoload_inventory find _select_item);
    wgp_vehcargoload_inventory set [wgp_vehcargoload_inventory find _select_item, "removeme"];
    wgp_vehcargoload_inventory = wgp_vehcargoload_inventory - ["removeme"];
    clearWeaponCargoGlobal vehicle player;
    clearItemCargoGlobal vehicle player;
    clearMagazineCargoGlobal vehicle player;
    clearBackpackCargoGlobal vehicle player;
    {
        private ["_newitemtype"];
        _newitemtype = _x call BIS_fnc_itemType;
	    switch (true) do {
	    	case (_newitemtype select 0 == "Magazine"): {vehicle player addMagazineCargoGlobal [_x, 1]};
	        case (_newitemtype select 0 == "Weapon"): {vehicle player addWeaponCargoGlobal [_x, 1]};
	        case (_newitemtype select 1 == "Backpack"): {vehicle player addBackpackCargoGlobal [_x, 1]};
	        default {vehicle player addMagazineCargoGlobal [_x, 1]};
	    };
    } foreach wgp_vehcargoload_inventory; 
    
    call compile preprocessFileLineNumbers "mp\veh_cargoload\fn_gui_update.sqf";
} else {
    if ((wgp_vehcargoload_items find _select_item) == -1) exitWith {
        hint ("We do not have " + _select_name + " in store, sorry!");
        
    };
    _totalmass = 0;
	{_totalmass = _totalmass + ([_x] call wgp_fnc_getItemMass); } foreach (weaponcargo vehicle player);
	{_totalmass = _totalmass + ([_x] call wgp_fnc_getItemMass); } foreach (itemcargo vehicle player);
	{_totalmass = _totalmass + ([_x] call wgp_fnc_getItemMass); } foreach (magazinecargo vehicle player);
	{_totalmass = _totalmass + ([_x] call wgp_fnc_getItemMass); } foreach (backpackcargo vehicle player);
    if (([_select_item] call wgp_fnc_getItemMass) + _totalmass > [vehicle player] call wgp_fnc_getCapacity) then {
        hint("You need " + str(round((([_select_item] call wgp_fnc_getItemMass) + _totalmass -([vehicle player] call wgp_fnc_getCapacity))*100)/100) +"kg more free space to fit " + _select_name + ".");
    } else {
	    // hint ("Added 1 " + _select_name);
	    _item_type = _select_item call BIS_fnc_itemType;
	    switch (true) do {
	    	case (_item_type select 0 == "Magazine"): {vehicle player addMagazineCargoGlobal [_select_item, 1]};
	        case (_item_type select 0 == "Weapon"): {vehicle player addWeaponCargoGlobal [_select_item, 1]};
	        case (_item_type select 1 == "Backpack"): {vehicle player addBackpackCargoGlobal [_select_item, 1]};
	        default {vehicle player addItemCargoGlobal [_select_item, 1]};
	    };
	    call wgp_fnc_gui_update;
    };
};