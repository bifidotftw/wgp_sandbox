﻿_t1 = diag_tickTime;
setTerrainGrid 3.125;

TF_give_microdagr_to_soldier = false;
tf_radio_channel_name = "TaskForceRadio";
tf_radio_channel_password = "123";
tf_same_sw_frequencies_for_side = true;
tf_same_lr_frequencies_for_side = true;
tf_no_auto_long_range_radio = false; //Give them at init so people can contact their buddies.



call compile preprocessFileLineNumbers "init_Variables.sqf";
{ // Init pV_bases, execute once
	private ["_trigger"];
	_trigger = _x select 0;
    _trigger setVariable ["Name", _x select 1];
    _trigger setVariable ["Value", _x select 2];	// Ticketbleed value
	_trigger setVariable ["Side", _x select 3];	// Current owner
	_trigger setVariable ["sideattack", _x select 3];	// Current attacker
    if (_x select 3 == Civilian) then {
		_trigger setVariable ["CapPoints", 0];	// Town Points
        _trigger setVariable ["CapCapacity", _x select 4];	// Neutral flags are at zero
    } else {
		_trigger setVariable ["CapPoints", _x select 4];	// Town Points
    	_trigger setVariable ["CapCapacity", _x select 4];	// Capped flags start at max
    }
} forEach pV_bases;
waituntil {!isnil "bis_fnc_init"};
if isServer then {
    {
        private ["_side", "_marker", "_name"];
        _side = _x select 1;
        _marker = _x select 0;
        _name = _x select 2;
        [_side, getMarkerPos _marker, _name] call bis_fnc_addRespawnPosition;
    } foreach pV_spawnpoints;
};



// if hasInterface then {waituntil {alive player}; sleep 0.2;};
execVM "flags\common\init.sqf";
execVM "flags\client\init.sqf";
execVM "flags\server\init.sqf";
[] spawn {
	waitUntil {!isNil "BIS_revive_initialized" && {BIS_revive_initialized}};
	BIS_revive_reviveDelayDefault = 10;		// time (in seconds) that it takes to revive a unit
	BIS_revive_reviveBleedOutDelayDefault = 600;	// time (in seconds) that it takes to bleed out
};
systemchat str (diag_tickTime - _t1);
call wgp_fnc_disablethermals;