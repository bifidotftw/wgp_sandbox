if !isServer exitwith {}; // Nono for clients.

_current_bleeder = Civilian;
while {true} do {

	_value_west = 0;
	_value_east = 0;
	_value_civ = 0;
	{
	    _trigger = _x select 0;
	    _side = _trigger getVariable "side";
	    _value = _trigger getVariable "value";
        if (_side == west) then {
            _value_west = _value_west + _value;
        } else {
            if (_side == east) then {
                _value_east = _value_east + _value;
            } else {
                _value_civ = _value_civ + _value
            }
        };
	} foreach pV_bases;
	
	if (_value_west > (_value_west + _value_east + _value_civ) /2) then {
        if (_current_bleeder != East) then {
            pV_playRadio = [[east, "HQ"], "USTicketBleedStart" + str(ceil random 2)];
            publicvariable "pv_playRadio";
            if hasInterface then {[[east, "HQ"], "USTicketBleedStart" + str(ceil random 2)] call wgp_fnc_misc_playRadio};
        	_current_bleeder = East;
        };
	    [east, -1] call aas_fnc_addtickets;
	} else {
        if (_value_east > (_value_west + _value_east + _value_civ) /2) then {
	        if (_current_bleeder != West) then {
	            pV_playRadio = [[west, "HQ"], "USTicketBleedStart" + str(ceil random 2)];
	            publicvariable "pv_playRadio";
                if hasInterface then {[[west, "HQ"], "USTicketBleedStart" + str(ceil random 2)] call wgp_fnc_misc_playRadio};
	            _current_bleeder = West;
	        };
	    	[west, -1] call aas_fnc_addtickets;
        } else {
	        if (_current_bleeder != Civilian) then {
	            pV_playRadio = [[_current_bleeder, "HQ"], "USTicketBleedEnd" + str(ceil random 2)];
	            publicvariable "pv_playRadio";
                if hasInterface then {[[_current_bleeder, "HQ"], "USTicketBleedEnd" + str(ceil random 2)] call wgp_fnc_misc_playRadio};
	            _current_bleeder = Civilian;
	        };
        };
    };

	sleep AAS_ticketbleed_interval;
};