
if (!hasInterface) exitWith {}; // Only clients need markers. Performance ftw.

private ["_side", "_colorWest", "_colorEast", "_colorNeutral", "_color", "_pre"];
_side = _this;

_colorWest = getarray (configfile >> "CfgMarkerColors" >> "ColorWest" >> "color");
{ _colorWest set [_foreachindex, call compile _x] } foreach _colorWest;
_colorEast = getarray (configfile >> "CfgMarkerColors" >> "ColorEast" >> "color");
{ _colorEast set [_foreachindex, call compile _x] } foreach _colorEast;
_colorNeutral = getarray (configfile >> "CfgMarkerColors" >> "ColorGrey" >> "color");
_color = switch (_side) do
	{ 
	  case west: {_colorWest};
	  case east: {_colorEast};
	  default {_colorNeutral}; 
	};
_tickets_west = str(p_AAS_tickets_west);
_tickets_east = str(p_AAS_tickets_east);
_pre = 4 - (count toArray str(p_AAS_tickets_west));
for "_x" from 1 to _pre do {
	_tickets_west = format ["%1%2", " ", _tickets_west];
    };
_pre = 4 - (count toArray str(p_AAS_tickets_east));
for "_x" from 1 to _pre do {
	_tickets_east = format ["%1%2", " ", _tickets_east];
    };


if (_side == west) then {
	uiNameSpace getVariable "WGP_AAS_TICKETS" ctrlsetStructuredText (
		parseText (
	    	"<t color='#ff0000'>" + _tickets_west + " </t><t shadow='2' shadowColor='#000000'><img size='1' image='" + team1_flag + "'/> " +
			"<img size='1' image='" + team2_flag + "'/></t> " + _tickets_east + "</t>"
	    )
	);
} else {
	uiNameSpace getVariable "WGP_AAS_TICKETS" ctrlsetStructuredText (
		parseText (
	    	_tickets_west + " <t shadow='2' shadowColor='#000000'><img size='1' image='" + team1_flag + "'/> " +
			"<img size='1' image='" + team2_flag + "'/></t><t color='#ff0000'> " + _tickets_east + "</t>"
	    )
	);
};
sleep 1;
uiNameSpace getVariable "WGP_AAS_TICKETS" ctrlsetStructuredText (
	parseText (
    	_tickets_west + " <t shadow='2' shadowColor='#000000'><img size='1' color='#ffffff' image='" + team1_flag + "'/> " +
		"<img size='1' color='#ffffff' image='" + team2_flag + "'/></t> " + _tickets_east
    )
);