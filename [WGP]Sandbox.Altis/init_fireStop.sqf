/*
	GrenadeStop v0.8 for ArmA 3 Alpha by Bake (tweaked slightly by Rarek)
	
	DESCRIPTION:
	Stops players from throwing grenades in safety zones.
	
	INSTALLATION:
	Move grenadeStop.sqf to your mission's folder. Then add the
	following line to your init.sqf file (create one if necessary):
	execVM "grenadeStop.sqf";
	
	CONFIGURATION:
	Edit the #defines below.
*/

#define MESSAGE "Stop shooting in the base! "

if (isDedicated) exitWith {};
waitUntil {!isNull player};

player addEventHandler ["Fired", {
    if (["m_base", getPos (_this select 0)] call BIS_fnc_inTrigger or ["m_base_1", getPos (_this select 0)] call BIS_fnc_inTrigger or ["m_base_2", getPos (_this select 0)] call BIS_fnc_inTrigger ) then {
		deleteVehicle (_this select 6);
		titleText [MESSAGE, "PLAIN", 3];
	};
}];

/*
allowFire = nil;
player addAction ["", 
			{ playSound3D ['a3\sounds_f\weapons\Other\dry9.wss', _this select 0]; },
	        "",
	        0,
	        false,
	        true,
	        "DefaultAction",
	        "isNil 'allowFire'"
      	 ];
player addEventHandler ["Fired", {
          if (["m_base", getPos (_this select 0)] call BIS_fnc_inTrigger
          or ["m_base_1", getPos (_this select 0)] call BIS_fnc_inTrigger
          or ["m_base_2", getPos (_this select 0)] call BIS_fnc_inTrigger ) then {
              allowFire = nil;
              deleteVehicle (_this select 6);
              titleText [MESSAGE, "PLAIN", 3];   
          } else {
              allowFire = True;
          };
}];

waitUntil {
    sleep 10;
    if (["m_base", getPos (_this select 0)] call BIS_fnc_inTrigger
    or ["m_base_1", getPos (_this select 0)] call BIS_fnc_inTrigger
    or ["m_base_2", getPos (_this select 0)] call BIS_fnc_inTrigger ) then {
          allowFire = nil;
    } else {
        allowFire = True;
    };
};*/