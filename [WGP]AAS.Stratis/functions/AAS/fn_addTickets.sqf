// Add Tickets serverside
// aas_fnc_addTickets
if !isServer exitwith{};

private ["_side", "_tickets"];

_side = _this select 0;
_tickets = _this select 1;

if (_side == west) then {
	p_AAS_Tickets_West = p_AAS_Tickets_West + _tickets;
    if (p_AAS_Tickets_West < 0) then {p_AAS_Tickets_West = 0};
} else {
	p_AAS_Tickets_East = p_AAS_Tickets_East + _tickets;
    if (p_AAS_Tickets_East < 0) then {p_AAS_Tickets_East = 0};  
};
pV_setTickets = if (_side == west) then {[west, p_AAS_Tickets_West]} else {[east, p_AAS_Tickets_East]};
publicvariable "pV_setTickets";

if hasInterface then {
    if (_side == west) then {
	    west execVM "flags\client\refreshTickets.sqf"
	} else {
	    east execVM "flags\client\refreshTickets.sqf"
    }
};