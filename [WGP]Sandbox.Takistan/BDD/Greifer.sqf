waitUntil { !(isNull player) };
waitUntil { time > 0 };
 
attached = false;
//Klassennamen der ziehbaren Objekte//Classnames of draggable objects 
private ["_ammo_classsnames", "_cfgvehicles", "_gueter"];
_gueter = [];
// Add any additional objects you want to be movable to "_ammo_classnames".
_ammo_classnames = ["Land_Pallet_MilBoxes_F","Land_PaperBox_closed_F","Land_PaperBox_open_empty_F","Land_PaperBox_open_full_F", "Land_MetalBarrel_empty_F","MetalBarrel_burning_F","Land_BarrelSand_grey_F","Land_BarrelWater_grey_F","Land_BarrelWater_F","Land_BarrelTrash_grey_F","Land_BarrelTrash_F"];

// Now we iterate over the entire cfgVehicles to find all ammoboxes there are. Big plus: This also checks for all addon-ammoboxes that you might use.
_cfgvehicles = configFile >> "cfgVehicles";
for "_i" from 0 to (count _cfgvehicles)-1 do {
	_vehicle = _cfgvehicles select _i;
	if (isClass _vehicle) then { // Sanity check
		private["_classname", "_vehicleclass"];
		_classname = configName(_vehicle); // Ammo box classname
		_vehicleclass = getText(_cfgvehicles >> _classname >> "vehicleClass"); // All ammo boxes have the "vehicleClass" == "Ammo"
		if (_vehicleclass == "Ammo") then {
			_ammo_classnames set [count _ammo_classnames, _classname];
		}
	};
};


while {true} do {
	/*
		We use a loop that checks for any valid draggable objects nearby every two seconds. Those get the "drag" option attached, while all objects that are now out of range will have theirs removed.
		TODO: Currently, the drag action gets removed for all objects, then added again for those nearby. A nicer solution would be to only remove it from those objects that have moved "out of range" since the last check.

	*/
	{
		private "_actionid";
		_actionid = _x getVariable "BDD_actionid";
		if (!isNil "_actionid") then {
			_x removeAction _actionid;
			_x setVariable ["BDD_actionid", nil];
		};
	} foreach _gueter;
	_gueter = nearestobjects [getpos player,_ammo_classnames,5] ;
	
	{
		_bdd = _x  addAction ["<t color=""#FFAD1F"" size='1'>" +"Drag", "BDD\ziehen.sqf", "", 1,true, true, "", "!attached and player distance _target<2 and (_target getvariable ['nodrag',true])"];
		_x setVariable ["BDD_actionid", _bdd]; // This variable stores the action index. Which we need to know later on which action to remove.
	} foreach _gueter;
	
sleep 2;
};

hintSilent "BDD - Bens Drag&Drop initialisiert.";