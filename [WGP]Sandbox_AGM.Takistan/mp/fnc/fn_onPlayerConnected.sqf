_netid = _this select 0;
_clientId = -1;

// diag_log format["DEBUG: OPC start"];
while{_clientId == -1} do {
	{
		if (owner _x == _netid) exitWith
		{
			_clientId = owner _x;
		};
	} forEach playableUnits;
	sleep .02;
};

{
    /* Prüft alle Gruppen mit individueller GroupID und sendet Änderungsaufforderung an einzelnen JIP-Client
     * 
     */
	private ["_group", "_InCustomGroup"];
	_group = group _x;
	_InCustomGroup = (leader _group) getVariable "CustomGroup";
    // diag_log format["DEBUG: Gruppe Suche"];
	if (!isNil "_InCustomGroup") then {
		if (_InCustomGroup) then {
            // diag_log format["DEBUG: Gruppe gefunden: " + groupID _group];
			pVAR_changeGroupID = [_group, groupID _group]; 
			_clientID publicVariableClient "pVAR_changeGroupID";
			pVAR_changeRank = [_x, rank _x];
			_clientID publicVariableClient "pVAR_changeRank";
		};
	};
} foreach playableUnits;

true;