if !isServer exitWith {}; // Only server will monitor flag process. Clients will be informed about necessary updates.

    
private ["_trigger","_unitcount","_name","_points","_marker","_text"];

while {true} do { 
    if (p_AAS_Tickets_East <= 0) exitwith {
        pV_hud_message = [["WGP_Victory", "NATO"]];
        publicvariable "pV_hud_message";
        sleep 3;
        pV_missionEnd = team1;
        publicVariable "pv_missionEnd";
        "victory" call BIS_fnc_endMission;
    };
    if (p_AAS_Tickets_West <= 0) exitwith {
        pV_hud_message = [["WGP_Victory", "CSAT"]];
        publicvariable "pV_hud_message";
        sleep 3;
        pV_missionEnd = team2;
        publicVariable "pv_missionEnd";
        "victory" call BIS_fnc_endMission;
    };
    
	for [{_i=0},{_i < count pV_bases}, {_i=_i+1}] do {

		_base = pV_bases select _i;
		_trigger = _base select 0;
		_name = _trigger getVariable "name";
		_side = _trigger getVariable "side";
		_sideattack = _trigger getVariable "sideattack";
        _capcapacity = _trigger getVariable "CapCapacity";
		_points = _trigger getVariable "CapPoints";
        _step = _base select 5;
        
		_numEast = 0;
		_numWest = 0;
		{
			_numEast = _numEast + ({isPlayer _x and side group _x == team2} count crew _x);
			_numWest = _numWest + ({isPlayer _x and side group _x == team1} count crew _x);
		} forEach list _trigger;
        
		if (((_numWest > _numEast and _side != team1) or (_numEast > _numWest and _side != team2)) and _i > 0 and _i < (count pV_bases) and _capcapacity > 0) then {
         // Triple-condition: Does one side have superiority? Does the flag belong to an enemy? Is it a mainbase?
        	
			_validroute = false;
            _prevstepcomplete = [];
			_newsideattack = civilian;
			if (_numWest > _numEast) then {
				_newsideattack = team1;
			} else {
				_newsideattack = team2;
			};
            {
                private "_trigger";
                _xtrigger = _x select 0;
                if (_step > 0) then {
                    if (_newsideattack == team1) then {
	                	if (_x select 5 == _step -1) then {
	                        if (_xtrigger getVariable "side" == team1) then {
	                            _prevstepcomplete set [count _prevstepcomplete, true];
	                        } else {
	                            _prevstepcomplete set [count _prevstepcomplete, false];
	                        };
	                    }
                    } else {
	                	if (_x select 5 == _step + 1) then {
	                        if (_xtrigger getVariable "side" == team2) then {
	                            _prevstepcomplete set [count _prevstepcomplete, true];
	                        } else {
	                            _prevstepcomplete set [count _prevstepcomplete, false];
	                        };
	                    }                        
                    };
                }
            } foreach pv_bases;
            
            {
                if (_trigger in _x) then {
                    {
                        if (_trigger != _x and (_x getVariable "side" == _newsideattack)) then {
                            _validroute = true;
                        }
                    } foreach _x;
                }
            } foreach routes;
			if (_validroute and !(false in _prevstepcomplete)) then {
                switch (_newsideattack) do {
                    case team1: {
	                    switch (_side) do {
	                        case team2: {
                                _points = _points - 2 * (_numWest - _numEast)
                            };
	                        case civilian: {
						        switch (_sideattack) do {
			                        case team1 : {
	                    					_points = _points + 2 * (_numWest - _numEast);
			                            };
			                        case team2 : {
	                                    if (_points > 0) then {
	                                        _points = _points - 2 * (_numWest - _numEast);
	                                    } else {
	            							_sideattack = teamneutral;
                                        }
			                        };
			                        default  {
	                                    if (_points <= 0) then {
	            							_sideattack = _newsideattack;
	                                    }
			                        };
			                    };
	                        };
	                    };
	                };
	                case team2 : 
                    {
	                    switch (_side) do {
	                        case team1: {
                                _points = _points - 2 * (_numEast - _numWest)
                            };
	                        case civilian: {
						        switch (_sideattack) do {
			                        case team2 : {
	                    					_points = _points + 2 * (_numEast - _numWest);
			                            };
			                        case team1 : {
	                                    if (_points > 0) then {
	                                        _points = _points - 2 * (_numEast - _numWest);
	                                    } else {
	            							_sideattack = teamneutral;
                                        }
			                        };
			                        default  {
	                                    if (_points <= 0) then {
	            							_sideattack = _newsideattack;
	                                    }
			                        };
			                    };
	                        };
	                    };
	                };
	                default {
	                };
                };
				if(_points <= 0 and _side != Civilian) then { // A Town has been neutralized!
					_side = civilian;
                    _points = 0;
                    pV_hud_message = [["WGP_TownNeutralized", _trigger getVariable "Name"]];
                    publicvariable "pV_hud_message";
                    if hasInterface then {pv_hud_message call wgp_fnc_hud_showMessage};
                };
                if (_points >= _capcapacity and _side == Civilian) then { // Zone has been fully captured
                	_side = _sideattack;
                	pV_hud_message = [["WGP_TownCaptured", [_trigger getVariable "Name", _side]]];
                    publicvariable "pV_hud_message";
                    pV_playRadio = [[_side, "HQ"], if (_side == west) then {"USTownCaptured" + str(ceil random 4)} else {"UKTownCaptured"  + str(ceil random 3)}];
                    publicvariable "pv_playRadio";
                    if hasInterface then {pv_hud_message call wgp_fnc_hud_showMessage};
                    if hasInterface then {[[_side, "HQ"],if (_side == west) then {"USTownCaptured"  + str(ceil random 4)} else {"UKTownCaptured" + str(ceil random 3)}] call wgp_fnc_misc_playRadio};
                    pV_playRadio = [[if (_side == west) then {east} else {west}, "HQ"], if (_side == west) then {"USTownLost"  + str(ceil random 4)} else {"UKTownLost"  + str(ceil random 3)}];
                    
                	if hasInterface then {[[if (_side == west) then {east} else {west}, "HQ"], if (_side == west) then {"USTownLost"  + str(ceil random 4)} else {"UKTownLost"  + str(ceil random 3)}] call wgp_fnc_misc_playRadio};
                    publicvariable "pv_playRadio";
                	[if (_side == west) then {east} else {west}, -30] call aas_fnc_addTickets;
				};
				_trigger setVariable ["cappoints", _points, true];
                _trigger setVariable ["side", _side, true];
                _trigger setVariable ["sideattack", _sideattack, true];
                pV_updateflag = [_i, _side, _sideattack, _points];
                publicvariable "pV_updateflag";
                if hasInterface then {execVM "flags\client\refreshMarkers.sqf"};
			};
		} else {
			if (_points < _capcapacity) then { // If points are below 100 and no cap is being done, slowly fill cap status
            	if (_side == West) then {
					_points = _points + 1 + 2 * (_numWest - _numEast) ;
                };
                if (_side == East) then {
                    _points = _points + 1 + 2 * (_numEast - _numWest) ;
                };
                if (_side == Civilian) then {
                    if (_points <= 0) then {
                        _points = 0;
                    } else  {
                   	 	_points = _points - 1;
                    }
                };
                
                if (_points > _capcapacity) then { _points = _capcapacity};
				_trigger setVariable ["CapPoints", _points, true];
        		//systemchat ("Points: " + str _points);
	            pV_AAS_refreshMarkers = true;
	            publicvariable "pV_AAS_refreshMarkers";
	            if hasInterface then {execVM "flags\client\refreshMarkers.sqf"};
			};
		};
            
	};
	sleep AAS_interval; // Check AAS progress every x seconds. Script takes about 0.01ms right now.
};