/* (Re-)creates the base markers.
 * Client-side.
 * 
 * 
 */
if (!hasInterface) exitWith {}; // Only clients need markers. Performance ftw.

private ["_trigger", "_name", "_side", "_sideattack", "_cappoints", "_capcapacity", "_color", "_colorattack", "_mflag", "_mborder", "_mcap"];
{
    _trigger = _x select 0;
    deleteMarkerLocal (_trigger getVariable "Marker_Flag");
    deleteMarkerLocal (_trigger getVariable "Marker_Border");
    deleteMarkerLocal (_trigger getVariable "Marker_Cap");
	_name = _trigger getVariable "name";
	_side = _trigger getVariable "side";
	_sideattack = _trigger getVariable "sideattack";
    _cappoints = _trigger getvariable "CapPoints";
    _capcapacity = _trigger getvariable "CapCapacity";
	
	_color = switch (_side) do 
	{ 
	  case team1: {team1_colormap};
	  case team2: {team2_colormap};
	  default {teamneutral_colormap}; 
	};
    _colorattack = switch (_sideattack) do
	{ 
	  case team1: {team1_colormap};
	  case team2: {team2_colormap};
	  default {teamneutral_colormap}; 
	};    
	
	_mflag = createmarkerLocal [_name, getpos _trigger];
	_mflag setmarkershapeLocal "ICON";
	_mflag setmarkertypeLocal "mil_box";
	_mflag setmarkertextLocal _name;
	_mflag setMarkerColorLocal _color;
	//_mflag setMarkerAlpha 0.5;

	_mborder = createmarkerLocal [_name + "_border", getpos _trigger];
	if ((triggerarea _trigger) select 3)
		then {_mborder setmarkershapeLocal "RECTANGLE" }
		else { _mborder setmarkershapeLocal "ELLIPSE"};
	_mborder setmarkersizeLocal [(triggerArea _trigger) select 0, (triggerArea _trigger) select 1];
	_mborder setMarkerDirLocal ((triggerArea _trigger) select 2) select 0;
	_mborder setMarkerColorLocal _color;
	_mborder setMarkerBrushLocal "Border";
    
	_mcap = createmarkerLocal [_name + "_cap", getpos _trigger];
	if ((triggerarea _trigger) select 3)
		then {_mcap setmarkershapeLocal "RECTANGLE" }
		else { _mcap setmarkershapeLocal "ELLIPSE"};
    _newX = ((triggerArea _trigger) select 0) * (_cappoints / _capcapacity);
    _newY = ((triggerArea _trigger) select 1) * (_cappoints / _capcapacity);
	_mcap setmarkerSizeLocal [_newX, _newY];
    _mcap setmarkerAlphaLocal 0.5;
	_mcap setMarkerDirLocal ((triggerArea _trigger) select 2) select 0;
	_mcap setMarkerColorLocal _colorattack;
	_mcap setMarkerBrushLocal "FDiagonal";
    
    
	_trigger setVariable ["Marker_Flag", _mflag];
	_trigger setVariable ["Marker_Border", _mborder];
	_trigger setVariable ["Marker_Cap", _mcap];
} forEach pV_bases;

// Draws base connections

if !isNil "marker_routes" then {
    {
	    deleteMarkerLocal _x;
	} foreach marker_routes;
};
marker_routes = [];
{
	private ["_base1","_base2","_xdif","_ydif","_xpos","_ypos","_pos","_marker"];

	_base1 = getPos (_x select 0);
    _base1side = (_x select 0) getVariable "Side";
	_base2 = getPos (_x select 1);
    _base2side = (_x select 1) getVariable "Side";

	_xdif = (_base1 select 0) - (_base2 select 0);
	_ydif = (_base1 select 1) - (_base2 select 1);

	_xpos = (_base1 select 0) - _xdif / 2;
	_ypos = (_base1 select 1) - _ydif / 2;

	_pos	= [_xpos,_ypos,0];
	_dir	= atan ( _xdif / _ydif );

	//player globalChat str _pos;

	_name 	= format ["CM_%1_%2",floor(time),floor(random(1000))];
	_type	= "RECTANGLE";
    
    _color = teamneutral_colormap;  
    if (_base1side == team1 and _base2side == team1) then {
         	_color = team1_colormap;
        } else {
    	if (_base1side == team2 and _base2side == team2) then {
             _color = team2_colormap;
        };
    };
	
	_marker = CreateMarkerLocal [_name, _pos];
	_marker setMarkerShapeLocal "RECTANGLE";
	_marker setMarkerBrushLocal "Solid";
	_marker setMarkerDirLocal _dir;
	_marker setMarkerSizeLocal [3, ((_base1 distance _base2) / 2) - 50 ];
	_marker setMarkerColorLocal _color;
	// _marker setMarkerAlphaLocal 100;
   	marker_routes set [count marker_routes, _marker];
} forEach routes;