comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_I_CombatUniform_shortsleeve";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "HandGrenade";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_PlateCarrierIA1_dgtl";
for "_i" from 1 to 1 do {this addItemToVest "200Rnd_65x39_cased_Box";};
for "_i" from 1 to 2 do {this addItemToVest "200Rnd_65x39_cased_Box_Tracer";};

this addHeadgear "H_HelmetIA";

comment "Add weapons";
this addWeapon "LMG_Mk200_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Hamr";
this addPrimaryWeaponItem "bipod_03_F_blk";

this addItemToVest "200Rnd_65x39_cased_Box";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "NVGoggles";