comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_I_CombatUniform";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "HandGrenade";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addVest "V_PlateCarrierIA1_dgtl";
for "_i" from 1 to 6 do {this addItemToVest "20Rnd_762x51_Mag";};
this addHeadgear "H_HelmetIA";

comment "Add weapons";
this addWeapon "srifle_EBR_F";
this addPrimaryWeaponItem "muzzle_snds_B";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_DMS";
this addPrimaryWeaponItem "bipod_03_F_blk";

this addItemToVest "20Rnd_762x51_Mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemRadio";
this linkItem "NVGoggles_INDEP";