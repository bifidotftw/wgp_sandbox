class CfgPatches {
  class wgp_aimod {
    units[] = {};
    weapons[] = {};
	requiredVersion = 0.1;
    requiredAddons[] = {agm_ai};
  };
};

class CfgAISkill {
  aimingAccuracy[] = {0,0, 1,1};	//{0,0,1,1}; Defaults
  aimingShake[]    = {0,0, 1,1};	//{0,0,1,1};
  aimingSpeed[]    = {0,0, 1,1};	//{0,0.5,1,1};
  commanding[]     = {0,0, 1,1};	//{0,0,1,1};
  courage[]        = {0,0, 1,1};	//{0,0,1,1};
  endurance[]      = {0,0, 1,1};	//{0,0,1,1};
  general[]        = {0,0, 1,1};	//{0,0,1,1};
  reloadSpeed[]    = {0,0, 1,1};	//{0,0,1,1};
  spotDistance[]   = {0,0, 1,1};	//{0,0.2,1,0.4};
  spotTime[]       = {0,0, 1,0.7};	//{0,0,1,0.7};
};