// if !isServer exitwith {};

private ["_vehicle", "_side", "_delay", "_vehclass"];
_vehicle = _this select 0;
_side = _this select 1;
_delay = _this select 2;
_vehclass = _this select 3;
if isNil {_vehicle getVariable "pos"} then {
	_vehicle setVariable ["pos", getPos _vehicle];
	_vehicle setVariable ["dir", getDir _vehicle];
};
_vehicle setVariable ["delay", _delay];
_vehicle setVariable ["side", _side];
_vehicle setVariable ["vehclass", _vehclass];


_vehicle addEventHandler ["Killed", {
    private ["_veh", "_pos", "_dir", "_side", "_vehclass", "_tickets"];
    _veh = _this select 0;
    _pos = _veh getVariable "pos";
    _dir = _veh getVariable "dir";
    _side = _veh getVariable "side";
    _vehclass = _veh getVariable "vehclass";
    
	_tickets = switch (_vehclass) do {
	  case "Jeep": {-2};
	  case "APC": {-5};
	  case "IFV": {-10};
	  case "Tank": {-15};
	  case "AAV": {-5};
	  case "Heli": {-10};
	  case "CAS_Heli": {-15};
	  case "Jet": {-15};
	  default {-1}; 
	};
    if isServer then {
    	[_side, _tickets] call AAS_fnc_addTickets;
    } else {
        pv_addTickets = [_side, _tickets]; publicVariableServer "pv_addTickets";
    };
    _veh spawn AAS_fnc_respawnVehicle;    
}];


_vehicle execVM "flags\client\vehicle_restrict.sqf";

_vehicle execVM "functions\vehicle\fn_helidoors.sqf";