comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "U_I_CombatUniform_shortsleeve";
this addItemToUniform "FirstAidKit";
for "_i" from 1 to 2 do {this addItemToUniform "HandGrenade";};
for "_i" from 1 to 2 do {this addItemToUniform "SmokeShell";};
this addItemToUniform "SmokeShellGreen";
this addVest "V_PlateCarrierIA1_dgtl";
for "_i" from 1 to 6 do {this addItemToVest "30Rnd_556x45_Stanag";};
for "_i" from 1 to 2 do {this addItemToVest "30Rnd_556x45_Stanag_Tracer_Green";};
for "_i" from 1 to 2 do {this addItemToVest "9Rnd_45ACP_Mag";};
this addHeadgear "H_HelmetIA";

comment "Add weapons";
this addWeapon "arifle_Mk20_F";
this addPrimaryWeaponItem "acc_pointer_IR";
this addPrimaryWeaponItem "optic_Hamr";
this addWeapon "hgun_ACPC2_F";
this addWeapon "Rangefinder";

this addItemToVest "30Rnd_556x45_Stanag";
this addItemToVest "9Rnd_45ACP_Mag";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "ItemWatch";
this linkItem "ItemRadio";
this linkItem "ItemGPS";
this linkItem "NVGoggles_INDEP";
