private ["_message", "_target", "_text_old", "_structext"];

_message = _this select 0;
_target = _this select 1;

if (if (!isNil "_target") then {playerside == _target} else {true}) then {

	if (typeName _message == "ARRAY") then {
        // Arrays indicate a message to be localized, so let's see what's up.
        _locstring = _message select 0;
        _locdata = _message select 1;
        _locdummy = [localize _locstring, _locdata];
        if (typeName _locdata == "ARRAY") then {
        	_locdummy = [localize _locstring];
            {
                _locdummy pushBack _x;
            } foreach _locdata;
        };
		if (isLocalized _locstring) then {
            
            _message = format _locdummy;
    	} else {
            diag_log ("ERROR: NOT LOCALIZED: " + _locstring);
            _message = _locstring + " " + str(_locdummy);
        };
    };
	_text_old = missionNameSpace getVariable "ui_AAS_message_text";
	if (isNil "_text_old") then {
	    missionNameSpace setVariable ["ui_AAS_message_text", [""]];
		_text_old = missionNameSpace getVariable "ui_AAS_message_text";
	};
	if (count _text_old > 3) then {
	    _text_old resize 3;
	};
	
	
	_message = ([daytime, "HH:MM"] call BIS_fnc_timeToString) + " " + _message;
	            
	_text_old = [_text_old, [_message],0] call BIS_fnc_arrayInsert;
	missionNameSpace setVariable ["ui_AAS_message_text", _text_old];
	
	_structext = "";
	{
	    if (_structext == "") then {
	        _structext = _x;
	    }else {
	    	_structext = _structext + "<br/>" + _x;
	    };
	} foreach _text_old;
	
	_structext = parseText _structext;
	uiNameSpace getVariable "WGP_AAS_MESSAGE" ctrlSetStructuredText(_structext);

};