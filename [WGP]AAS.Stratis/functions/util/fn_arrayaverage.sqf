/* ----------------------------------------------------------------------------
Function: wgp_fnc_arrayaverage

Description:
    A function used to return the count of unique in an array.

Parameters:
    Array

Example:
    (begin example)
    _types = [0,0,1,1,1,1] call wgp_fnc_arrayaverage
    _types2 = ["a","a","a","b","b","c"] call wgp_fnc_arrayaverage
    (end)

Returns:
    Array element counts. For above examples:
		_types = [[0,2],[1,4]];
        _types2 = [["a",3],["b",2],["c",1]];

Author:
    Original CBA_fnc_getArrayElements by Rommel && sbsmac
	Modified by Senshi

---------------------------------------------------------------------------- */

private ["_sum", "_return"];

_sum = 0;
{
	_sum = _sum + _x;
} foreach _this;
_return = _sum / count _this;
_return