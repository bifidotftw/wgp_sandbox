// Team Settings
team1 = west;
team1_name = "UK";
team1_flag = "A3\data_F\flags\flag_uk_co.paa";
team1_flagsmall = "A3\UI_F\data\map\markers\flags\uk_ca.paa";
team1_color = getarray (configfile >> "CfgMarkerColors" >> "ColorWest" >> "color");
{team1_color set [_foreachindex, call compile _x] } foreach team1_color;
team1_colormap = "ColorWest";

team2 = east;
team2_name = "CSAT";
team2_flag = "A3\data_F\flags\flag_csat_co.paa";
team2_flagsmall = "A3\UI_F\data\map\markers\flags\uk_ca.paa";
team2_color = getarray (configfile >> "CfgMarkerColors" >> "ColorEast" >> "color");
{team2_color set [_foreachindex, call compile _x] } foreach team2_color;
team2_colormap = "ColorEast"; 

teamneutral = civilian;
teamneutral_flag = "A3\data_F\flags\flag_white_co.paa";
teamneutral_flagsmall = teamneutral_flag;
teamneutral_color = getarray (configfile >> "CfgMarkerColors" >> "ColorGrey" >> "color");
teamneutral_colormap = "ColorBlack";

wgp_aas_depotmarkers = [
	["team1_veh_depot", team1, "LandVehicle"], 
    ["team1_plane_depot", team1, "Plane"], 
    ["team1_heli_depot", team1, "Helicopter"],
	["team2_veh_depot", team2, "LandVehicle"], 
    ["team2_plane_depot", team2, "Plane"], 
    ["team2_heli_depot", team2, "Helicopter"]
];
{
	_x select 0 setMarkerAlphaLocal 0;
} foreach wgp_aas_depotmarkers;


/*
	Defines the capturable bases. First and last are not capturable (mainbase).
	Run only on init/join.
*/
pV_bases = [
//		Trigger0	Name1					Value2		Side3		Captime4	Order5
	[	base_0,		"Stratis Airfield",		0,			team1,		-1,			0], // 0
	[	base_1,		"Agia Marina",			100,		team1,	240,		1], // 1
	[	base_2,		"Kill Farm",			100,		teamneutral,	240,		2], // 2
	[	base_3,		"Camp Rogain",			100,		team1,	240,		2], // 3
	[	base_4,		"Transmitter Station",	100,		team2,	240,		3], // 4
	[	base_5,		"Kamino Firing Range",	0,			team2,		-1,			4] //5
];



pV_spawnpoints = [
	["spawn_west_inf", team1, "Barracks"],
    ["spawn_west_veh", team1, "Vehicle Depot"],
    ["spawn_west_air", team1, "Airfield"],
	["spawn_east_inf", team2, "Barracks"],
    ["spawn_east_veh", team2, "Vehicle Depot"],
    ["spawn_east_air", team2, "Airfield"],
    ["spawn_west_agia", team1, "Agia Marina"],
    ["spawn_west_killfarm", team1, "Kill Farm"],
    ["spawn_west_rogain", team1, "Camp Rogain"],
    ["spawn_west_transmitter", team1, "Transmitter Station"],
    ["spawn_east_agia", team2, "Agia Marina"],
    ["spawn_east_killfarm", team2, "Kill Farm"],
    ["spawn_east_rogain", team2, "Camp Rogain"],
    ["spawn_east_transmitter", team2, "Transmitter Station"]
];



/*
	Defines the connections between bases
	Run only on init/join
*/
routes = [
	[	base_0,	base_1],
	[	base_1,	base_2],
	[	base_1,	base_3],
	[	base_3,	base_4],
	[	base_2,	base_4],
	[	base_4,	base_5]
];



// Generic AAS settings
// Score reward for capping flags
AAS_reward_cappoint = 1; // Point for every cappoint deducted
AAS_reward_capfull = 25; // Bonus for full cap


// "Speed" settings
AAS_interval = 0.1; // Delay between AAS status checks in sec. Lower values will lead to faster town capture.
p_AAS_Tickets_West = paramsArray select 0; // Tickets per Team. Defined as parameter at mission launch.
p_AAS_Tickets_East = paramsArray select 0;
AAS_ticketbleed_interval = 10;
pV_ticketbleed = civilian;

// Loadouts = West, East, Guerilla, Civilian
loadouts_setup = [
	[ // WEST Faction
    	//Name, filename, max per squad, players needed per kit, limit per players in team
		["Squad Leader",		"Squadleader", 	1,	2,	2],
		["Medic",				"Medic", 		2,	2,	-1],
		["Automatic Rifleman",	"AR", 			1,	6,	-1],
		["Rifleman",			"Rifleman", 	99, -1,	-1],
		["Grenadier",			"Grenadier", 	1,	4,	-1],
		["NLAW Anti-Tank",		"LAT", 			1,	4,	-1],
		["Titan Anti-Tank",		"HAT", 			1,	2,	16],
		["Anti-Air",			"AA", 			1,	2,	16],
		["Engineer",			"Engineer", 	99,	2,	8],
		["Marksman",			"Marksman", 	1,	6,	-1],
		["Sniper",				"Sniper", 		99,	2,	16],
		["Crewman",				"Crewman", 		99,	-1,	-1],
		["Pilot",				"Pilot", 		99,	-1,	-1]
    ],
    [ // EAST Faction
		["Squad Leader",		"Squadleader",	1,	2,	8],
		["Medic",				"Medic",		2,	2,	-1],
		["Automatic Rifleman",	"AR",			1,	4,	-1],
		["Rifleman",			"Rifleman",		99,	-1,	-1],
		["Grenadier",			"Grenadier",	1,	4,	-1],
		["NLAW Anti-Tank",		"LAT",			1,	4,	-1],
		["Titan Anti-Tank",		"HAT",			1,	6,	2],
		["Anti-Air",			"AA",			1,	2,	2],
		["Engineer",			"Engineer",		99,	6,	2],
		["Marksman",			"Marksman",		1,	6,	-1],
		["Sniper",				"Sniper",		99,	2,	2],
		["Crewman",				"Crewman",		99,	-1,	-1],
		["Pilot",				"Pilot",		99,	-1,	-1]
    ],
    [ // GUER Faction
    ],
    [ // CIV Faction
    ]
];