if !isServer exitWith{};
// Status update for JIP player
"pV_RequestClientId" addPublicVariableEventHandler {
   _JIP_netid = owner (_this select 1);
   
   [_JIP_netid] spawn AAS_fnc_onPlayerConnected;  
};

wgp_aas_scr_setupEH = execVM "flags\server\setupEventhandler.sqf";
wgp_aas_scr_refreshmarkers = execVM "flags\client\refreshMarkers.sqf";
wgp_aas_scr_updateflags = execVM "flags\server\updateFlags.sqf";
wgp_aas_scr_monitorticketbleed = execVM "flags\server\monitor_ticketbleed.sqf";

{
    _base = _x;
    _trigger = _x select 0;
    _step = _x select 5;
    
    
    if (_step == 0 or _foreachindex == count pv_Bases -1) then {
		_radius = ((triggerArea _trigger) select 0) max ((triggerArea _trigger) select 1);
		_list = nearestObjects [(getPos _trigger), ["Building", "Wall"], _radius];
		{;
		    if ([_trigger, _x] call BIS_fnc_inTrigger) then {
		        _x allowdamage false;
		    }
		} foreach _list;
    };   
    
} foreach pV_bases;