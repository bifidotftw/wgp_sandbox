#include "defines.hpp";	
disableSerialization;
createDialog "WGP_SQUADMGR"; 

private ["_squadlist", "_memberlist", "_squadnametext", "_groups", "_squadinfo"];
_squadlist = finddisplay WGP_IDD_SQUADMGR displayctrl WGP_IDC_SQUADMGR_List;
_memberlist = finddisplay WGP_IDD_SQUADMGR displayctrl WGP_IDC_SQUADMGR_Members_List;
_squadnametext =  finddisplay WGP_IDD_SQUADMGR displayctrl WGP_IDC_SQUADMGR_EditName;


private["_groups"];
_groups = [];
{
	if (!(group _x in _groups) && side group _x == playerside) then {
		private["_group","_CustomGroup"];
		_group = group _x;
		_CustomGroup = _group getVariable "CustomGroup";
		if (!isNil "_CustomGroup") then {
			_groups = _groups + [_group];
		};
	};
} foreach playableUnits;

{
	_squadlist lbAdd groupID _x;
    _squadinfo = squadParams leader _x select 0;
    if (!isNil "_squadinfo") then {
    	if (count _squadinfo > 0) then {
            _squadlist lbSetPicture [(lbSize _squadlist) -1, _squadinfo select 4];
        };
    };
} foreach _groups;
_squadinfo = squadParams player select 0;
if (!isNil "_squadinfo") then {
    if (count _squadinfo > 0) then {_squadnametext ctrlSetText (_squadinfo select 0)};
};
if (count _groups > 0) then {
	_squadlist lbSetCurSel 0;
};

/*-----------------------------------------------------------------	*/
/* Dialog anzeigen*/
WGP_SQUADMGR_Button = -1;
WGP_SQUADMGR_Selection = -1;
waitUntil {!dialog };
/*-----------------------------------------------------------------	*/

// Select existing squad
if ((WGP_SQUADMGR_Button == 0) && (WGP_SQUADMGR_Selection != -1)) then {
    _group = _groups select WGP_SQUADMGR_Selection;
    {
    	lbAdd [_memberlist, name _x];
    } foreach units _group;
};


// Create new
if (WGP_SQUADMGR_Button == 1) then {
    
    _isnew = true;
    {
        if (WGP_SQUADMGR_newname == groupID _x) exitwith {
            systemchat (format [localize "WGP_Squadmgr_ExistsAlready", WGP_SQUADMGR_newname]);
            _isnew = false;
        } ;
        
    } foreach _groups;
    if (_isnew) then {
	    pV_changeRank = [player, "LIEUTENANT"]; publicVariable "pV_changeRank";
	    pV_changeRank call wgp_fnc_setRank;
		private["_group"];
		_group = createGroup playerside;
		[player] join _group;
	    pV_changeGroupID = [_group, WGP_SQUADMGR_newname]; publicVariable "pV_changeGroupID";
	    pV_changeGroupID call wgp_fnc_setGroupID;
		_group setVariable ["CustomGroup",groupID _group, true];
		
		systemchat (format [localize "WGP_Squadmgr_Created", WGP_SQUADMGR_newname]);
        WGP_SQUADMGR_newname = nil;
    };
};


/* Join */
if ((WGP_SQUADMGR_Button == 2) && (WGP_SQUADMGR_Selection != -1)) then {
	private["_group"];
	_group = _groups select WGP_SQUADMGR_Selection;
    if (group player != _group) then {
	    pV_changeRank = [player, "CORPORAL"]; publicVariable "pv_changeRank";
	    pV_changeRank call wgp_fnc_setRank;
	
		[player] join _group;
		systemchat (format [localize "WGP_Squadmgr_Joined", groupID _group]) ;
    } else {
        systemchat (format [localize "WGP_SquadMgr_InAlready", groupID _group]);
    };
};
/* Leave */
if (WGP_SQUADMGR_Button == 3) then {
    pV_changeRank = [player, "PRIVATE"]; publicVariable "pVAR_changeRank";
    pV_changeRank call wgp_fnc_setRank;
	systemchat (format [localize "WGP_Squadmgr_Left", groupID (group player)]);
	[player] join grpNull;
};

WGP_SQUADMGR_Button = nil;
WGP_SQUADMGR_Selection = nil;
