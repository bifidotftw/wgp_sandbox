comment "Exported from Arsenal by [WGP]Senshi";

comment "Remove existing items";
removeAllWeapons this;
removeAllItems this;
removeAllAssignedItems this;
removeUniform this;
removeVest this;
removeBackpack this;
removeHeadgear this;
removeGoggles this;

comment "Add containers";
this forceAddUniform "BWA3_Uniform_Fleck";
for "_i" from 1 to 4 do {this addItemToUniform "AGM_Bandage";};
for "_i" from 1 to 2 do {this addItemToUniform "AGM_Morphine";};
this addItemToUniform "AGM_EarBuds";
for "_i" from 1 to 2 do {this addItemToUniform "BWA3_DM25";};
this addItemToUniform "BWA3_20Rnd_762x51_G28_Tracer";
this addVest "BWA3_Vest_Leader_Fleck";
for "_i" from 1 to 4 do {this addItemToVest "BWA3_20Rnd_762x51_G28";};
for "_i" from 1 to 4 do {this addItemToVest "BWA3_20Rnd_762x51_G28_Tracer";};
this addHeadgear "BWA3_OpsCore_Fleck";

comment "Add weapons";
this addWeapon "BWA3_G27";
this addPrimaryWeaponItem "BWA3_acc_LLM01_irlaser";
this addPrimaryWeaponItem "BWA3_optic_RSAS";
this addWeapon "AGM_Vector";

comment "Add items";
this linkItem "ItemMap";
this linkItem "ItemCompass";
this linkItem "AGM_Altimeter";
this linkItem "tf_anprc152_10";
this linkItem "ItemGPS";