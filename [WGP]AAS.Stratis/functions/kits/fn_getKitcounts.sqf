/*
 * If called on a player:
 * Returns the number of kits in the player's squad and the whole team
 * 
 * If called on a side:
 * Returns nil for squad and the whole team, still in array. 
 * 
*/


private ["_playercount", "_playerside", "_kitcount_team", "_kitcount_squad", "_kits_team"];

_playercount = count allplayers;
_playerside = if (typename _this  == "SIDE") then {_this} else {side group _this};
_kitcount_team = [];
_kitcount_squad = [];

_kits_team = switch (_playerside) do {
	case west: {loadouts_setup select 0};
	case east: {loadouts_setup select 1};
	case resistance: {loadouts_setup select 2};
	default {loadouts_setup select 3};
};

{
    _kitcount_squad pushback 0;
    _kitcount_team pushback 0;
} foreach _kits_team;


{
    private ["_side", "_kitidx"];
    _side = side group _x;
    _kitidx = _x getVariable "playerkitid";
    if (! isNil ("_kitidx")) then {
	    _kitcount_team set [_kitidx, (_kitcount_team select _kitidx) + 1]; 
	    if (group _x == group player) then {
	   		_kitcount_squad set [_kitidx, (_kitcount_squad select _kitidx) + 1];
	    };
    };
    
} foreach allplayers;

[_kitcount_team, _kitcount_squad]